<?php

return array(

    'IOSUser'     => array(
        'environment' => 'production',
        'certificate' => app_path().'/apns/user/blisscars247-passenger-production-no-passphrase.pem',
       // 'environment' => 'development',
       // 'certificate' => app_path().'/apns/user/blisscars247-passenger-development-no-passphrase.pem',
        'passPhrase'  => '',
        'service'     => 'apns'
    ),
    'IOSProvider' => array(
      'environment' => 'production',
      'certificate' => app_path().'/apns/provider/blisscars247-passenger-production-no-passphrase.pem',
      //'environment' => 'development',
      //'certificate' => app_path().'/apns/provider/blisscars247-driver-development-no-passphrase.pem',
       'passPhrase'  => '',
        'service'     => 'apns'
    ),
    'AndroidUser' => array(
        'environment' => 'production',
        'apiKey'      => 'AAAA03siFwI:APA91bHgfxZvtv8j2DTL5CwLeZXGtnS-meMvS7S8STDWgdUap-Cz8jxzRezwY4gmxlpSQe_41SoGNJcfexTie25U_qvnnAw3WVPs3_2pOqN61jjal_Jg9aupkXxO1iRrboqxEmFGdbd6',
        'service'     => 'gcm'
    ),
    'AndroidProvider' => array(
        'environment' => 'production',
        'apiKey'      => 'AAAA03siFwI:APA91bHgfxZvtv8j2DTL5CwLeZXGtnS-meMvS7S8STDWgdUap-Cz8jxzRezwY4gmxlpSQe_41SoGNJcfexTie25U_qvnnAw3WVPs3_2pOqN61jjal_Jg9aupkXxO1iRrboqxEmFGdbd6',
        'service'     => 'gcm'
    )

);