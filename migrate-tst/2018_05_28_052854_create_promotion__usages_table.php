<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion__usages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('provider_id');
            $table->integer('promocode_id');
            $table->enum('status', ['ADDED', 'USED','EXPIRED']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion__usages');
    }
}
