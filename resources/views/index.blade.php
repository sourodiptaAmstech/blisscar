@extends('user.layout.main')

@section('content')

<div class="main-content">
        <!-- Header Starts -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> <a class="navbar-brand" href="/"><span class="logo-st">BLISS CARS</span></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="{{ url('/login') }}">Ride Now</a>
                        </li>
                        <li>
                            <a href="{{ url('/provider/login') }}">Driver Account</a>
                        </li>
                        <li>
                            <a href="{{ url('/provider/login') }}" class="bold"><span class="drive">BECOME A DRIVER</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header Ends -->
        <!-- Content Wrapper Starts -->
        <div class="content-wrapper">
            <div class="slider-sec">

                <div class="slide-box-outer">
                    <div class="slide-box bg-img" style="background-image: url('{{asset('img-1.png')}}');">
                        <div class="slide-overlay"></div>
                        <div class="slide-content text-center">
                            <h1>
                                TAKE A <br> <span class="main-txt">BLISS CARS</span> <br> FOR A BETTER EXPERIENCE
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="slide-box-outer">
                    <div class="slide-box bg-img second-slide" style="background-image: url('{{asset('img-2.png')}}');">
                        <div class="slide-overlay"></div>
                        <div class="slide-content text-center">
                            <h1>
                                <span class="main-txt">BLISS CARS</span> <br> FOR A BETTER EXPERIENCE
                            </h1>
                            <div class="app-demo">
                                {{--<h2 class="app-demo-tit">App Demo</h2>--}}
                                <div>

                                    {{-- <a href="https://play.google.com/store/apps/details?id=com.uturn.user&hl=en" class="app-icon"><img src="{{asset('android-app.png')}}"></a>
                                    <a href="https://itunes.apple.com/us/app/uturn-user/id1266825567?mt=8" class="app-icon"><img src="{{asset('ios-app.png')}}"></a> --}}
                                    <a href="#" class="app-icon"><img src="{{asset('android-app.png')}}"></a>
                                    <a href="#" class="app-icon"><img src="{{asset('ios-app.png')}}"></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-box-outer">
                    <div class="slide-box bg-img" style="background-image: url('{{asset('img-3.png')}}');">
                        <div class="slide-overlay"></div>
                        <div class="slide-content text-center">
                            <h1>
                                <span class="main-txt">BLISS CARS</span> <br> FOR A BETTER EXPERIENCE
                            </h1>
                            <div class="app-demo">
                                {{--<h2 class="app-demo-tit">App Demo</h2>--}}
                                <div>
                                    {{-- <a href="https://play.google.com/store/apps/details?id=com.uturn.user&hl=en" class="app-icon"><img src="{{asset('android-app.png')}}"></a>
                                    <a href="https://itunes.apple.com/us/app/uturn-user/id1266825567?mt=8" class="app-icon"><img src="{{asset('ios-app.png')}}"></a> --}}
                                    <a href="#" class="app-icon"><img src="{{asset('android-app.png')}}"></a>
                                    <a href="#" class="app-icon"><img src="{{asset('ios-app.png')}}"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-box-outer">
                    <div class="slide-box bg-img" style="background-image: url('{{asset('img-4.png')}}');">
                        <div class="slide-overlay"></div>
                        <div class="slide-content text-center">
                            <h1>
                                <span class="main-txt">BLISS CARS</span> <br> FOR A BETTER EXPERIENCE
                            </h1>
                            <div class="app-demo">
                                {{--<h2 class="app-demo-tit">App Demo</h2>--}}
                                <div>
                                    {{-- <a href="https://play.google.com/store/apps/details?id=com.uturn.user&hl=en" class="app-icon"><img src="{{asset('android-app.png')}}"></a>
                                    <a href="https://itunes.apple.com/us/app/uturn-user/id1266825567?mt=8" class="app-icon"><img src="{{asset('ios-app.png')}}"></a> --}}
                                    <a href="#" class="app-icon"><img src="{{asset('android-app.png')}}"></a>
                                    <a href="#" class="app-icon"><img src="{{asset('ios-app.png')}}"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-box-outer">
                    <div class="slide-box bg-img" style="background-image: url('{{asset('img-5.png')}}');">
                        <div class="slide-overlay"></div>
                        <div class="slide-content text-center">
                            <h1>
                                <span class="main-txt">BLISS CARS</span> <br> FOR A BETTER EXPERIENCE
                            </h1>
                            <div class="app-demo">
                                {{--<h2 class="app-demo-tit">App Demo</h2>--}}
                                <div>
                                    {{-- <a href="https://play.google.com/store/apps/details?id=com.uturn.user&hl=en" class="app-icon"><img src="{{asset('android-app.png')}}"></a>
                                    <a href="https://itunes.apple.com/us/app/uturn-user/id1266825567?mt=8" class="app-icon"><img src="{{asset('ios-app.png')}}"></a> --}}
                                    <a href="#" class="app-icon"><img src="{{asset('android-app.png')}}"></a>
                                    <a href="#" class="app-icon"><img src="{{asset('ios-app.png')}}"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Section Starts -->
            <div class="about-section">
                <div class="container">
                    <div class="about-sec-inner bg-img row">
                        <div class="col-md-12">
                            <div class="about-left">
                                <h2 class="about-tit">About</h2>
                                <p class="about-txt">BLISS CARS was formed by drivers with a great wealth of experience and the desire to improve the experience for both the driver and the customer. As insiders in the industry, we know first hand how unfair treatment of the drivers can lead to low performance, which ultimately affects our customers.</p>
                                <p class="about-txt">We aim to treat our drivers better by charging them 50% lower commissions than the competition and providing dedicated round the clock customer support.</p>
                                <div class="about-list row m-0">
                                    <div class="pull-left about-list-left">
                                        <i class="fa fa-car"></i>
                                    </div>
                                    <div class="about-list-right">
                                        <h4 class="about-list-tit">Only The Best Drivers</h4>
                                        <p class="about-list-txt">BLISS CARS  only accepts the highest rated drivers</p>
                                    </div>                                    
                                </div>
                                <div class="about-list row m-0">
                                    <div class="pull-left about-list-left">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <div class="about-list-right">
                                        <h4 class="about-list-tit">DISCOUNT</h4>
                                        <p class="about-list-txt">Join to enjoy 10% off all rides in the TWIN CITIES</p>
                                    </div>                                    
                                </div>
                                <div class="about-list row m-0">
                                    <div class="pull-left about-list-left">
                                        <i class="fa fa-star"></i>
                                    </div>
                                    <div class="about-list-right">
                                        <h4 class="about-list-tit">In-App OPEN Tipping</h4>
                                        <p class="about-list-txt">Tip your Driver for 5 star service</p>
                                    </div>                                    
                                </div>
                                <div class="about-list row m-0">
                                    <div class="pull-left about-list-left">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <div class="about-list-right">
                                        <a href="tel:651-300-6353"><h4 class="about-list-tit">Here For You</h4></a>
                                        <p class="about-list-txt">24/7 live phone, email and text support</p>
                                    </div>                                    
                                </div>
                            </div>
                        </div>

                        <!--<div class="col-md-6 text-center">
                            <img src="{{asset('about.jpg')}}" class="about-img">
                        </div>-->
                    </div>
                </div>
            </div>
            <!-- About Section Ends -->
            <!-- City View Starts -->
            <div class="city-view bg-img" style="background-image: url('{{asset('city.jpg')}}');">
                <div class="slide-overlay"></div>
                <div class="slide-content text-center">
                    <h1>BLISS CARS IS NOW IN THE TWIN CITIES</h1>
                </div>
            </div>
            <!-- City View Ends -->
            <div class="footer">
                <div class="foot-top row m-0">
                    <div class="container">
                        <div class="col-md-12 text-center">
                           {{--  <a style="margin-right: 10px" href="https://m.facebook.com/UTURNRIDE/"><img src="facebookLg.png"></a>
                            <a style="margin-right: 10px" href="https://twitter.com/UturnRide?lang=en"><img src="twit.png"></a>
                            <a href="https://www.instagram.com/uturnride/"><img src="instagram.png"></a> --}}
                            <a style="margin-right: 10px" href="#"><img src="facebookLg.png"></a>
                            <a style="margin-right: 10px" href="#"><img src="twit.png"></a>
                            <a href="#"><img src="instagram.png"></a>
                        </div>
                        <div class="col-md-12 text-center">
                            <a href="/blog" class="foot-item">Blog</a>
                            {{-- <a href="mailto:uturn@uturnride.com ?subject=Uturn%Contact" data-toggle="tooltip" data-placement="bottom" title="Contact uturn@uturnride.com"class="foot-item">Contact US</a>
                            <a href="https://u5370211.ct.sendgrid.net/wf/click?upn=liIZAyVtxMPs7vx6wA5BfIus67CdIptbEmlxMuhkwCu7Kd9GhDuv7SoS3bdrIJ98NtBNguEeVK9k2dR6q1kGZWvnA9NjMsNbe9-2FNrObfW-2BgtSXkq9nLdsjhbubr5zlh9vU1XZg7MRl15kw9H0UX6pg-3D-3D_pbADWQERWNF7ZkEVfLjtqhIaPrDb7pA8NN-2Bsxj2Nno9qldlUVfAloqLyArBdpJag4oMV5sVwlMMxff1PbxbWdqaSKSACLFEFMLI06-2FOgP1a37cJ5eSBJB0e-2FW6uzSpf-2FTamFtJbvtZjEf1nfopqLSTC-2FE3op07zZKR-2FpaTYIckXW-2F0b5iHrAOwP2fVy7JP6YpTiM6RejDiozG8W0-2BkIDB-2Fi-2FdnnTyUD4qomqEHttwSOLR06SUKx4EeyZOoplN2qQ " class="foot-item">Privacy Policy</a>
                            <a href="https://termsfeed.com/terms-conditions/cb345017f157ad625695f3ac4a758dd1" class="foot-item">Terms &amp; Conditions</a> --}}
                            <a href="mailto:blisscars247@gmail.com ?subject=BlissCars%Contact" data-toggle="tooltip" data-placement="bottom" title="Contact blisscars247@gmail.com"class="foot-item">Contact US</a>
                            <a href="#" class="foot-item">Privacy Policy</a>
                            <a href="#" class="foot-item">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
                <div class="foot-btm text-center">
                    <div class="container">
                        <p class="copy-txt">BLISS CARS  2020-All rights reserved</p>
                    </div>                    
                </div>
            </div>
        </div>
        <!-- Content Wrapper Ends -->
    </div>
@endsection