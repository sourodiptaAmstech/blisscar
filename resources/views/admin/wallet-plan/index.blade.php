@extends('admin.layout.base')

@section('title', 'Wallet Plans')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
            Wallet Plans
                @if(Setting::get('demo_mode', 0) == 1)
                <span class="pull-right">(*personal information hidden in demo)</span>
                @endif
            </h5>
            <a href="{{ route('admin.wallet-plan.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Plan</a>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Duration</th>
                        <th>Amount</th>
                        <th>Currency</th>
                        <th>status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($plans as $index => $plan)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $plan->duration }}</td>
                        <td>{{ $plan->amount }}</td>
                        <td>{{$plan->currency}}</td>
                        <td>
                           
                                @if($plan->status == 1)
                                    <label class="btn btn-block btn-primary">Active</label>
                                @else
                                    <label class="btn btn-block btn-warning">InActive</label>
                                @endif
                            
                        </td>
                        <td>
                            <form action="{{ route('admin.wallet-plan.destroy', $plan->id) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="{{ route('admin.wallet-plan.edit', $plan->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                    <th>ID</th>
                        <th>Duration</th>
                        <th>Amount</th>
                        <th>Currency</th>
                        <th>status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection