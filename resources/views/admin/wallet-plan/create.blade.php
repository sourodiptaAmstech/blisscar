@extends('admin.layout.base')

@section('title', 'Add Wallet Plan ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.wallet-plan.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Wallet Plan</h5>

            <form class="form-horizontal" action="{{route('admin.wallet-plan.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="duration" class="col-xs-12 col-form-label">Select Duration</label>
					<div class="col-xs-10">
						<select class="form-control" name="duration" id="duration">
						<option value="7 days">7 days</option>
						<option value="30 days">30 days</option>
                        </select>
					</div>
				</div>

				


					<div class="form-group row">
					<label for="amount" class="col-xs-12 col-form-label">Amount(₵)</label>
					<div class="col-xs-10">
						<input class="form-control" type="text"  name="amount" required id="amount" placeholder="Amount">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-12 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Plan</button>
						<a href="{{route('admin.wallet.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
