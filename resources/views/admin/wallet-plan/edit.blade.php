@extends('admin.layout.base')

@section('title', 'Edit Wallet Plan ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.wallet-plan.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Edit Wallet Plan</h5>

            <form class="form-horizontal" action="{{route('admin.wallet-plan.update', $walletPlan->id)}}" method="POST" enctype="multipart/form-data" role="form">
				{{csrf_field()}}
				{{ method_field('PATCH') }}
				<div class="form-group row">
					<label for="duration" class="col-xs-12 col-form-label">Select Duration</label>
					<div class="col-xs-10">
						<select class="form-control" name="duration" id="duration">
						<option value="7 days" <?php if($walletPlan->duration == '7 days'){ echo 'selected';}?>>7 days</option>
						<option value="30 days" <?php if($walletPlan->duration == '30 days'){ echo 'selected';}?>>30 days</option>
                        </select>
					</div>
				</div>

				


					<div class="form-group row">
					<label for="amount" class="col-xs-12 col-form-label">Amount(₵)</label>
					<div class="col-xs-10">
						<input class="form-control" type="text"  name="amount" required id="amount" placeholder="Amount" value="{{$walletPlan->amount}}">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-12 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Edit Plan</button>
						<a href="{{route('admin.wallet.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
