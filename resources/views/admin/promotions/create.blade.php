@extends('admin.layout.base')

@section('title', 'Add Promotion ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.promotion.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Add Promotion</h5>

            <form class="form-horizontal" action="{{route('admin.promotion.store')}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <div class="form-group row">
					<label for="discount" class="col-xs-2 col-form-label">Discount</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" min="0" step="1" value="{{ old('discount') }}" name="discount" required id="discount" placeholder="Discount">
					</div>
				</div>

				<div class="form-group row">
					<label for="expiration" class="col-xs-2 col-form-label">Expiration</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" value="{{ old('expiration') }}" name="expiration" required id="expiration" placeholder="Expiration">
					</div>
				</div>

                <div class="form-group row">
                    <label for="content" class="col-xs-2 col-form-label">Promotion Content</label>
                    <div class="col-xs-10">
                        <textarea rows="6" class="form-control" name="content" required id="content" placeholder="Promotion Content">{{ old('content') }}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="activated_fordriver" class="col-xs-2 col-form-label">Activated</label>
                    <div class="col-xs-10">
                        <select class="form-control" id="activated_fordriver" name="activated_fordriver">
                            <option value="1">YES</option>
                            <option value="0">NO</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="driver_discout_criteria_time" class="col-xs-2 col-form-label">Discount Criteria (Duration in Hours)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="number" min="0" step="1" value="{{ old('driver_discout_criteria_time') }}" name="driver_discout_criteria_time" id="driver_discout_criteria_time" placeholder="DURATION">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="driver_discout_criteria_distance" class="col-xs-2 col-form-label">Discount Criteria (Distance in Miles)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="number" min="0" step="1" value="{{ old('driver_discout_criteria_distance') }}" name="driver_discout_criteria_distance" id="driver_discout_criteria_distance" placeholder="DISTANCE">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-12 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Add Promotion</button>
                        <a href="{{route('admin.promotion.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
