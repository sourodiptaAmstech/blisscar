@extends('admin.layout.base')

@section('title', 'Add Wallet ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.wallet.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Wallet</h5>

            <form class="form-horizontal" action="{{route('admin.wallet.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="select_provider" class="col-xs-12 col-form-label">Select Provider</label>
					<div class="col-xs-10">
						<select class="form-control" name="provider_id" id="select_provider">
                        @foreach($providers as $index => $provider)
                            <option value="{{$provider->id}}">{{$provider->first_name}}{{$provider->last_name}}</option>
                            @endforeach
                        </select>
					</div>
				</div>

				<div class="form-group row">
					<label for="select_plan" class="col-xs-12 col-form-label">Select Plan</label>
					<div class="col-xs-10">
						<select class="form-control" name="plan_id" id="select_plan">
                        @foreach($walletPlans as $index => $walletPlan)
                            <option value="{{$walletPlan->id}}">{{$walletPlan->duration}}</option>
                            @endforeach
                        </select>
					</div>
				</div>

				<div class="form-group row">
					<label for="valid_from" class="col-xs-12 col-form-label">Valid From</label>
					<div class="col-xs-10">
						<input class="form-control" type="date"  name="validation_start" required id="valid_from" placeholder="Last Name">
					</div>
				</div>
					<div class="form-group row">
					<label for="zipcode" class="col-xs-12 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Provider Wallet</button>
						<a href="{{route('admin.wallet.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
