@extends('admin.layout.base')

@section('title', 'Provider Wallet ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Provider Wallet
                
                @if(Setting::get('demo_mode', 0) == 1)
                <span class="pull-right">(*personal information hidden in demo)</span>
                @endif
            </h5>
            <a href="{{ route('admin.wallet.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Wallet</a>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Valid From</th>
                        <th>Valid Till</th>
                        <th>Validity Duration</th>
                        <th>Valid</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach($wallets as $index => $wallet)
                    <tr>
                    

                       <td>{{ $index + 1 }}</td>
                        <td>{{ $wallet->provider['first_name'] }} {{ $wallet->provider['last_name'] }}</td>
                        <td>{{ $wallet->provider['email'] }}</td>
                        <td>{{ $wallet->provider['mobile'] }}</td>
                        <td>{{ $wallet->validation_start }}</td>
                        <td>{{ $wallet->validation_end }}</td>
                        <td>{{ $wallet->days }}</td>
                        <td>
                           
                                @if($wallet->valid == 1)
                                    <label class="btn btn-block btn-primary">Yes</label>
                                @else
                                    <label class="btn btn-block btn-warning">No</label>
                                @endif
                            
                        </td>
                        <td>
                            <form action="{{ route('admin.wallet.destroy', $wallet->id) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="{{ route('admin.wallet.edit', $wallet->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                <a href="{{ route('admin.wallet.show', $wallet->id) }}" class="btn btn-primary"><i class="fa fa-history"></i>Transactions</a>
                                <button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                    <th>ID</th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Valid From</th>
                        <th>Valid Till</th>
                        <th>Validity Duration</th>
                        <th>Valid</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection