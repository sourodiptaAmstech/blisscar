@extends('admin.layout.base')

@section('title', 'Airport Instruction')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Airport Instruction</h5>
                <!--- <a href="{{ route('admin.airportinstructionpush.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New News or Event</a> --->
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>News or Event Title</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($airportInstructionPushs as $index => $airportInstructionPush)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$airportInstructionPush->title}}</td>
                            <td>{{$airportInstructionPush->description}}</td>
                            <td>{{$airportInstructionPush->status}}</td>
                            <td>
                                
                                 <a href="{{ route('admin.airportinstructionpush.edit', $airportInstructionPush->id) }}" class="btn btn-default"><i class="fa fa-pencil"></i> Edit</a>
                               
                            </td>
                        </tr>
                    @endforeach  
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>News And Events</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection