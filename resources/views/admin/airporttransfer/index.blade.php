@extends('admin.layout.base')

@section('title', 'Airport Transfer')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Airport Transfer</h5>
                <!--- <a href="{{ route('admin.airporttransfer.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New News or Event</a> --->
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Passenger Name</th>
                            <th>Pickup Locations</th>
                            <th>Drop Locations</th>
                            <th>Flight Info</th>
                            <th>Pickup Time</th>
                            <th>Hand Luggage</th>
                            <th>Suitcase Luggage</th>
                            <th>No of Passenger</th>
                            <th>Service Type</th>

                          <!--  <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($AirportUserRequests as $index => $airporttransfer)
                  
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$airporttransfer->first_name}} {{$airporttransfer->last_name}}</td>
                            <td>{{$airporttransfer->s_address}}</td>
                            <td>{{$airporttransfer->d_address}}</td>
                            <td>{{$airporttransfer->flight_info}}</td>
                            <td>{{$airporttransfer->schedule_at}}</td>
                            <td>{{$airporttransfer->hand_luggage}}</td>
                            <td>{{$airporttransfer->suitcase_luggage}}</td>
                            <td>{{$airporttransfer->no_of_passenger}}</td>
                            <td>{{$airporttransfer->provider_name}}</td>
                         <!--   <td>
                                
                                 <a href="{{ route('admin.airporttransfer.edit', $airporttransfer->id) }}" class="btn btn-default"><i class="fa fa-pencil"></i> Edit</a>
                               
                            </td> -->
                            <td style="line-height: 33px;">
                              

                                <a href="{{ route('admin.airportransfer.details', $airporttransfer->id) }}" class="btn btn-info">View</a>
                            </td>
                        </tr>
                    @endforeach  
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Passenger Name</th>
                            <th>Pickup Locations</th>
                            <th>Drop Locations</th>
                            <th>Flight Info</th>
                            <th>Pickup Time</th>
                            <th>Hand Luggage</th>
                            <th>Suitcase Luggage</th>
                            <th>No of Passenger</th>
                            <th>Service Type</th>

                        <!--    <th>Action</th> -->
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection