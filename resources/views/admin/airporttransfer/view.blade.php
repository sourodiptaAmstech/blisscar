@extends('admin.layout.base')

@section('title', 'Airport Transfer Details ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h4>Airport Transfer Details</h4>
            <a href="{{ route('admin.airporttransfer.index') }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            <div class="row">
                <div class="col-md-6">
                    <dl class="row">
                        <dt class="col-sm-4">User Name :</dt>
                        <dd class="col-sm-8">{{ $request->user->first_name }} {{ $request->user->last_name }}</dd>
                        <dt class="col-sm-4">Pickup Address :</dt>
                        <dd class="col-sm-8">{{ $request->s_address ? $request->s_address : '-' }}</dd>

                        <dt class="col-sm-4">Destination Address :</dt>
                        <dd class="col-sm-8">{{ $request->d_address ? $request->d_address : '-' }}</dd>
                        <dt class="col-sm-4">Ride Scheduled Time :</dt>
                        <dd class="col-sm-8">
                            @if($request->schedule_at != "0000-00-00 00:00:00")
                                {{ date('jS \of F Y h:i:s A', strtotime($request->schedule_at)) }} 
                            @else
                                - 
                            @endif
                        </dd>
                        <dt class="col-sm-4">Flight Information :</dt>
                        <dd class="col-sm-8">
                            {{ $request->flight_info ? $request->flight_info : '--' }}
                        </dd>

                        <dt class="col-sm-4">Hand Luggage :</dt>
                        <dd class="col-sm-8">{{$request->hand_luggage}}</dd>
                        <dt class="col-sm-4">Suitcase Luggage :</dt>
                        <dd class="col-sm-8">{{$request->suitcase_luggage}}</dd>

                        <dt class="col-sm-4">No of Passenger :</dt>
                        <dd class="col-sm-8">{{$request->no_of_passenger}}</dd>

                        <dt class="col-sm-4">Service Type :</dt>
                        <dd class="col-sm-8">{{$request->service_type->provider_name}}</dd>
                    </dl>
                </div>
                <div class="col-md-6">
                    <div id="map"></div>
                </div>
            </div>

            <hr>
            <h4>Assign Driver</h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <div class="col-xs-10">
                            <select class="form-control" id="providers" name="providers">
                                
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="map"></div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('styles')
<style type="text/css">
    #map {
        height: 450px;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    var map;
    var zoomLevel = 11;

    function initMap() {

        map = new google.maps.Map(document.getElementById('map'));
        var base_url = window.location.origin;
        var marker = new google.maps.Marker({
            map: map,
            icon: base_url+'/laraval/blisscars/asset/img/marker-start.png',
            anchorPoint: new google.maps.Point(0, -29)
        });
        


         var markerSecond = new google.maps.Marker({
            map: map,
            icon: base_url+'/laraval/blisscars/asset/img/marker-end.png',
            anchorPoint: new google.maps.Point(0, -29)
        });

        var bounds = new google.maps.LatLngBounds();
        var waypoints=[];

        <?php
        // "var javascript_array = '".json_encode($MultipleWayPoints)."';"; 
       // echo "var javascript_array = ". $js_array . ";\n";
        ?>
        //console.log("------------------------------------");
       // console.log(JSON.parse(javascript_array));
        // javascript_array=JSON.parse(javascript_array);
        // for(var i=0; i<Object.keys(javascript_array).length;i++){
        //    // console.log(javascript_array[i].latitude);
        //     waypoints.push({
        //         location:new google.maps.LatLng(javascript_array[i].w_latitude, javascript_array[i].w_longitude),
        //         stopover:true
        //     });
        // /*    new google.maps.Marker({
        //     map: map,
        //     icon: base_url+'/laraval/blisscars/asset/img/marker-end.png',
        //    anchorPoint: new google.maps.Point(0, -29),
        //     position:new google.maps.LatLng(javascript_array[i].w_latitude, javascript_array[i].w_longitude)
        //     });*/
        //     }
        
     
        source = new google.maps.LatLng({{ $request->s_latitude }}, {{ $request->s_longitude }});

        destination = new google.maps.LatLng({{ $request->d_latitude }}, {{ $request->d_longitude }});

        marker.setPosition(source);
        markerSecond.setPosition(destination);

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true, preserveViewport: true});
        directionsDisplay.setMap(map);

        directionsService.route({
            origin: source,
            waypoints:waypoints,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(result, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                console.log(result);
                directionsDisplay.setDirections(result);

                marker.setPosition(result.routes[0].legs[0].start_location);
                markerSecond.setPosition(result.routes[0].legs[0].end_location);
            }
        });

        @if($request->provider && $request->status != 'COMPLETED')
        var markerProvider = new google.maps.Marker({
            map: map,
            icon: base_url+"/laraval/blisscars/asset/img/marker-car.png",
            anchorPoint: new google.maps.Point(0, -29)
        });

        provider = new google.maps.LatLng({{ $request->provider->latitude }}, {{ $request->provider->longitude }});
        markerProvider.setVisible(true);
        markerProvider.setPosition(provider);
        console.log('Provider Bounds', markerProvider.getPosition());
        bounds.extend(markerProvider.getPosition());
        @endif

        bounds.extend(marker.getPosition());
        bounds.extend(markerSecond.getPosition());
        map.fitBounds(bounds);
    }

    $(document).ready(function(){
        $("#providers").select2();

        var s_latitude = {{$request->s_latitude}};
        var s_longitude = {{$request->s_longitude}};
        var service_type = {{$request->service_type_id}};
        var d_latitude = {{$request->d_latitude}};
        var d_longitude = {{$request->d_longitude}};
        var distance = {{$request->distance}};
        $.ajax({
            url: "{{ route('admin.driver.assign') }}",
            method:"GET",
            data:{s_latitude: s_latitude,s_longitude: s_longitude, service_type: service_type,d_latitude: d_latitude, d_longitude: d_longitude,distance: distance},
            success: function(response){ 
                console.log(response);
                // $('#providers').empty();
                // var options = '<option value="">Select</option>';
                // $.each(response.data, function(key, value) {
                //   options += '<option value=""></option>';
                // });
                // $('#providers').append(options);
            },
            error: function(response){ 
            },
        });
           
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_KEY') }}&libraries=places&callback=initMap" async defer></script>
@endsection