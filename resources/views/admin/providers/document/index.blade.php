@extends('admin.layout.base')

@section('title', 'Provider Documents ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Provider Service Type Allocation</h5>
            <div class="row">
                <div class="col-xs-12">
                    @if($ProviderService->count() > 0)
                    <hr><h6>Allocated Services :  </h6>
                    <table class="table table-striped table-bordered dataTable">
                        <thead>
                            <tr>
                                <th>Service Name</th>
                                <th>Service Number</th>
                                <th>Service Make</th>
                                <th>Service Year</th>
                                <th>Service Model</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             
                                @foreach($ProviderService as $service)
                                  @foreach($Services as $serve)
                                <tr>
                                <td>{{ $serve->name }}</td>
                                <td>{{ $service->service_number }}</td>                                
                                <td>{{ $service->service_make }}</td>
                                <td>{{ $service->service_year }}</td>
                                <td>{{ $service->service_model }}</td>
                                <td>
                                    <form action="{{ route('admin.provider.document.service', [$Provider->id, $service->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-large btn-block">Delete</a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Service Name</th>
                                <th>Service Number</th>
                                <th>Service Model</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    @endif
                    <hr>
                </div>
                <form action="{{ route('admin.provider.document.store', $Provider->id) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="col-xs-3">
                        <select class="form-control input" name="service_type" required>
                            @forelse($ServiceTypes as $Type)
                            <option value="{{ $Type->id }}">{{ $Type->name }}</option>
                            @empty
                            <option>- Please Create a Service Type -</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <input type="text" required name="service_number" class="form-control" placeholder="Number (CY 98769)">
                    </div>
                    <div class="col-xs-3">
                        <input type="text" required name="service_make" class="form-control" placeholder="Make (Audi)">
                    </div>
                    <div class="col-xs-3">
                        <input type="text" required name="service_year" class="form-control" placeholder="Year (1970)">
                    </div>
                    <div class="col-xs-3">
                        <input type="text" required name="service_model" class="form-control" placeholder="Model (R8)">
                    </div>
                    <div class="col-xs-3">
                        <button class="btn btn-primary btn-block" type="submit">Add Service</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="box box-block bg-white">
            <h5 class="mb-1">Provider Documents</h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Document Type</th>
                        <th>Front Image</th>
                       <!-- <th>Back Image</th> -->
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <head>
<style>
img {
	width:auto;
	max-width:20%;
	height:auto;
}
</style>

                    @foreach($Provider->documents as $Index => $Document)
                    <tr>
                        
                        <td>{{ $Index + 1 }}</td>
                        <td>
                        @if(isset($Document) && isset($Document->document->name))
                        {{ $Document->document->name }}
                           @else
                           {{'NA'}}
                           @endif
                            </td>
                       <td>
                           @if(isset($Document) && isset($Document->front_url))
                           <img src="{{asset('storage/'.$Document->front_url)}}"  style="width:100%">
                           @else
                           <img src="{{ url('asset/img/no_img.png') }}"  style="width:100%">
                           @endif
                       </td>
                  <!--     <td>
                       @if(isset($Document) && isset($Document->back_url))
                           <img src="{{asset('storage/'.$Document->back_url)}}"  style="width:100%">
                           @else
                           <img src="{{ url('asset/img/no_img.png') }}"  style="width:100%">
                           @endif
                       </td> -->
                        <td> 
                            @if(isset($Document) && isset($Document->status))
                            {{ $Document->status }}
                           @else
                           {{'NA'}}
                           @endif
                        </td>
                        <td>
                            <div class="input-group-btn">
                            @if(isset($Document) && isset($Document->id))
                            <a href="{{ route('admin.provider.document.edit', [$Provider->id, $Document->id]) }}"><span class="btn btn-success btn-large">View</span></a>
                           @else
                           {{'NA'}}
                           @endif
                                
                                <button class="btn btn-danger btn-large" form="form-delete">Delete</button>
                                @if(isset($Document) && isset($Document->id))
                                <form action="{{ route('admin.provider.document.destroy', [$Provider->id, $Document->id]) }}" method="POST" id="form-delete">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>
                           @else
                           {{'NA'}}
                           @endif
                                
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Document Type</th>
                        <th>Front Image</th>
                       <!-- <th>Back Image</th> -->
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection