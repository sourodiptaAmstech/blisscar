<?php

namespace App\Http\Controllers\Resource;

use Auth;
use Setting;
use Exception;
use DB;
use App\Provider;
use Carbon\Carbon;
use App\AirportUserRequests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SendPushNotification;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AirportTransferResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if($request->has('app')) {
            
            // news_type =0 rider 
            // news_type =1 Driver 
          //  $sql="SELECT * FROM airport_user_requests aur left JOIN providers p on aur.user_id =p.id";
            $AirportUserRequests = AirportUserRequests::getAll()->orderBy('created_at' , 'desc')->get();  
            
            return response()->json([
                'AirportUserRequests' => $AirportUserRequests,
            ]);
        } else {
            $sql="SELECT aur.*,p.first_name,p.last_name,st.name, st.provider_name, 
            st.image,st.image_select,st.image_unselect,st.capacity,st.suitcases_luggage,
            st.hand_luggage,st.insure_price,st.fixed,st.price,st.min_price,st.minute,st.distance,
            st.calculator,st.description,st.min_waiting_time,st.min_waiting_charge,st.status
            FROM airport_user_requests aur 
            left JOIN providers p on aur.user_id =p.id
            left JOIN service_types st on aur.service_type_id =st.id";

            $AirportUserRequests=DB::select($sql);
         //   print_r( $AirportUserRequests); exit;

          //  print_r($SQLS); exit;
          //  $AirportUserRequests = AirportUserRequests::orderBy('created_at' , 'desc')->get();
            return view('admin.airporttransfer.index', compact('AirportUserRequests'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $airportInstructionPushs = AirportUserRequests::orderBy('created_at' , 'desc')->get(); 
       
        if(count($airportInstructionPushs) > 0)
         return redirect()->route('admin.airportinstructionpush.index')->with('flash_success', 'Airport Instruction Already Exist.');       
        else
            return view('admin.airportinstructionpushs.create');
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        try{
            // $request->input('camera_video');
           // $newsnevent = NewsAndEvents::create($request->all());
            
             $airportinstructionpush=new AirportUserRequests();
             $airportinstructionpush->title=$request->input('title');
             $airportinstructionpush->description=htmlspecialchars($request->input('description'));                    
             $airportinstructionpush->save();          
                    
            // to do for push
            
          /* $allDeviceResponse = (new SendPushNotification)->SendToRiderAll($request);
            
            if($allDeviceResponse){
                AirportInstructionPushs::where('id',$airportinstructionpush->id)->update([
                    'status' => "SUCCESS"
                ]);
            }  */          
            return redirect('admin/airportinstructionpush')->with('flash_success','News or Event Saved Successfully');
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'News or Event Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return AirportUserRequests::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            
            
            $airportinstructionpush = AirportUserRequests::findOrFail($id);
            return view('admin.airportinstructionpushs.edit',compact('airportinstructionpush'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        try {
            AirportUserRequests::where('id',$id)->update([
                    'title' => $request->title,
                    'description' => htmlspecialchars($request->input('description')),
                ]);
            
           /*  $allDeviceResponse = (new SendPushNotification)->SendToRiderAll($request);
            
            if($allDeviceResponse){
                AirportInstructionPushs::where('id',$id)->update([
                    'status' => "SUCCESS"
                ]);
            }*/
            return redirect()->route('admin.airportinstructionpush.index')->with('flash_success', 'News or Event Updated Successfully');    
        } 

        catch (Exception $e) {
            return back()->with('flash_error', 'News or Event Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewsAndEvent  $NewsAndEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if(Setting::get('demo_mode', 0) == 1) {
            return back()->with('flash_error', 'Disabled for demo purposes! Please contact us at info@appoets.com');
        }
        try {
            AirportUserRequests::find($id)->delete();
            return back()->with('message', 'News or Event deleted successfully');
        } 
        catch (Exception $e) {
            return back()->with('flash_error', 'News or Event Not Found');
        }
    }

    

    public function airportTransferDetails($id)
    {
        try {
            $request = AirportUserRequests::findOrFail($id);
            //dd($request);
            return view('admin.airporttransfer.view', compact('request'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    public function driverAssign(Request $request){
        $latitude = $request->s_latitude;
        $longitude = $request->s_longitude;
        $service_type = $request->service_type;
        $dlatitude = $request->d_latitude;
        $dlongitude = $request->d_longitude;
        $distance = $request->distance;

        $DestinationSetProviders = Provider::with('service') ->where('status', 'approved') ->join('driver_towards_destinations', 'providers.id', '=', 'driver_towards_destinations.provider_id')
              ->select(DB::Raw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'providers.id')
            ->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
            ->whereHas('service', function($query) use ($service_type){
                        $query->where('status','active');
                        $query->where('service_type_id',$service_type);
                    })
                ->select(DB::Raw("(6371 * acos( cos( radians('$dlatitude') ) * cos( radians(driver_towards_destinations.d_lat) ) * cos( radians(driver_towards_destinations.d_lng) - radians('$dlongitude') ) + sin( radians('$dlatitude') ) * sin( radians(driver_towards_destinations.d_lat) ) ) ) AS distance"),'providers.id','driver_towards_destinations.provider_id')
                 ->whereRaw("(6371 * acos( cos( radians('$dlatitude') ) * cos( radians(driver_towards_destinations.d_lat) ) * cos( radians(driver_towards_destinations.d_lng) - radians('$dlongitude') ) + sin( radians('$dlatitude') ) * sin( radians(driver_towards_destinations.d_lat) ) ) ) <= 2")
            ->orderBy('distance')
            ->get();
        
        // ****************************************************** SELECTED DESTINATION DRIVER WHO NOT THEIR TWOWARDS DESTINATION WITH DESTINATION FROM 'PROVIDER' AND "TOWARDS DESTINATION" *****
         
        $GeneralProviders = Provider::with('service') ->where('status', 'approved')
            ->leftJoin('driver_towards_destinations', 'providers.id', '=', 'driver_towards_destinations.provider_id')
            ->whereNull('driver_towards_destinations.provider_id')
            ->select(DB::Raw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'providers.id')
            ->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
           ->whereHas('service', function($query) use ($service_type){
                        $query->where('status','active');
                        $query->where('service_type_id',$service_type);
                    })
            ->orderBy('distance')
               ->get();
        
        // **************************************** MARGE TOW TYPE PROVIDER ***************************************************************
        $Providers  = array();
        foreach ($DestinationSetProviders as $key => $Provider1) {
            $Providers[]=$Provider1;
        }

        foreach ($GeneralProviders as $key => $Provider1) {
            $Providers[]=$Provider1;
        }
        
        return response()->json([
                'providers' => $Providers,
            ]);
    }

}
