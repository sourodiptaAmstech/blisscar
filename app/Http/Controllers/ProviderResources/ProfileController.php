<?php

namespace App\Http\Controllers\ProviderResources;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Controllers\Controller;

use DB;
use Auth;
use Setting;
use Storage;    
use Exception;
use Carbon\Carbon;

use App\Provider;
use App\Promocode;
use App\PromotionUsages;
use App\ProviderProfile;
use App\UserRequests;
use App\ProviderService;
use App\Fleet;
use App\ServiceType;
use App\EngineStandard;

class ProfileController extends Controller
{
    /**
     * Create a new user instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('provider.api', ['except' => ['show', 'store', 'available', 'location_edit', 'location_update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            Auth::user()->provider = ProviderService::where('provider_id',Auth::user()->id)->first();
            Auth::user()->service = ProviderService::where('provider_id',Auth::user()->id)->with('service_type')->get();
            Auth::user()->allservice = ServiceType::get();
            Auth::user()->allengine = EngineStandard::get();
            Auth::user()->fleet = Fleet::find(Auth::user()->fleet);
            Auth::user()->currency = Setting::get('currency', '$');
            Auth::user()->sos = Setting::get('sos_number', '911');
            //engine name 

            foreach(Auth::user()->service as $key=>$val){
                $engineName=EngineStandard::where("id",$val->engine_standard)->first();
                Auth::user()->service[$key]->engineName=$engineName->name;
                Auth::user()->provider->engineName=$engineName->name;
            }
        

            //return Auth::user()->provider;
            return Auth::user();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'mobile' => 'required',
                'avatar' => 'mimes:jpeg,bmp,png',
                'language' => 'max:255',
                'address' => 'max:255',
                'address_secondary' => 'max:255',
                'city' => 'max:255',
                'country' => 'max:255',
                'postal_code' => 'max:255',
                'service_type.*' => 'required',
                'service_number' => 'required',
                'service_model' => 'required',
                'service_make' => 'required',
                'service_year' => 'sometimes|year'
                ]);

            

        try {
          
            $Provider = Auth::user();

            if($request->has('first_name')) 
                $Provider->first_name = $request->first_name;

            if($request->has('last_name')) 
                $Provider->last_name = $request->last_name;

            if ($request->has('mobile'))
                $Provider->mobile = $request->mobile;
            if ($request->has('isdCode'))
                $Provider->isdCode = $request->isdCode;

            if ($request->hasFile('avatar')) {
                Storage::delete($Provider->avatar);
                $Provider->avatar = $request->avatar->store('app/public/provider/profile');
                $Provider->avatar=str_replace("app/public/", "", $Provider->avatar); 
            }

           

            if($request->has('service_type')) {
                if($Provider->service) {
                    if($Provider->service->service_type_id != implode(',', $request->service_type)) {
                        $Provider->status = 'banned';
                    }

                     foreach ($request->service_type as  $value) {
                    $ProviderService = ProviderService::find(Auth::user()->id);
                    $ProviderService->service_type_id = $value;
                    $ProviderService->service_number = $request->service_number;
                    $ProviderService->service_model = $request->service_model;
                    $ProviderService->service_make = $request->service_make;
                    $ProviderService->service_year = $request->service_year;
                    $ProviderService->save();
                     }

                } else {
                    foreach ($request->service_type as  $value) {
                    $ProviderService = new ProviderService;
                    $ProviderService->service_type_id = $value;
                    $ProviderService->service_number = $request->service_number;
                    $ProviderService->service_model = $request->service_model;
                    $ProviderService->service_year = $request->service_year;
                    $ProviderService->service_make = $request->service_make;

                    $ProviderService->save();
                     }
                    $Provider->status = 'banned';
                }
            }

            if($Provider->profile) {
                $Provider->profile->update([
                        'language' => $request->language ? : $Provider->profile->language,
                        'address' => $request->address ? : $Provider->profile->address,
                        'address_secondary' => $request->address_secondary ? : $Provider->profile->address_secondary,
                        'city' => $request->city ? : $Provider->profile->city,
                        'country' => $request->country ? : $Provider->profile->country,
                        'postal_code' => $request->postal_code ? : $Provider->profile->postal_code,
                    ]);
            } else {
                ProviderProfile::create([
                        'provider_id' => $Provider->id,
                        'language' => $request->language,
                        'address' => $request->address,
                        'address_secondary' => $request->address_secondary,
                        'city' => $request->city,
                        'country' => $request->country,
                        'postal_code' => $request->postal_code,
                    ]);
            }


            $Provider->save();

            return redirect(route('provider.profile.index'));
        }

        catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Provider Not Found!'], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('provider.profile.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

       

        $this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'mobile' => 'required',
                'avatar' => 'mimes:jpeg,bmp,png',
                'language' => 'max:255',
                'address' => 'max:255',
                'address_secondary' => 'max:255',
                'city' => 'max:255',
                'country' => 'max:255',
                'postal_code' => 'max:255',
                'service_type.*' => 'required',
                'engine_standard' => 'sometimes'
            ]);

        try {

            $Provider = Auth::user();

            if($request->has('first_name')) 
                $Provider->first_name = $request->first_name;

            if($request->has('last_name')) 
                $Provider->last_name = $request->last_name;

            if ($request->has('mobile'))
                $Provider->mobile = $request->mobile;
            
            if ($request->has('isdCode'))
                $Provider->isdCode = $request->isdCode;

            if ($request->hasFile('avatar')) {
                Storage::delete($Provider->avatar);
                $Provider->avatar = $request->avatar->store('app/public/provider/profile');
                $Provider->avatar=str_replace("app/public/", "", $Provider->avatar); 
            }

            if($Provider->profile) {
                $Provider->profile->update([
                        'language' => $request->language ? : $Provider->profile->language,
                        'address' => $request->address ? : $Provider->profile->address,
                        'address_secondary' => $request->address_secondary ? : $Provider->profile->address_secondary,
                        'city' => $request->city ? : $Provider->profile->city,
                        'country' => $request->country ? : $Provider->profile->country,
                        'postal_code' => $request->postal_code ? : $Provider->profile->postal_code,
                    ]);
            } else {
                ProviderProfile::create([
                        'provider_id' => $Provider->id,
                        'language' => $request->language,
                        'address' => $request->address,
                        'address_secondary' => $request->address_secondary,
                        'city' => $request->city,
                        'country' => $request->country,
                        'postal_code' => $request->postal_code,
                    ]);
            }
//dd($Provider->service); exit;
            if($Provider->service) {
        $ProviderService = ProviderService::where('provider_id', $Provider->id)->delete();
		foreach (explode(',', $request->service_type) as  $value) {
                    $ProviderService = new ProviderService;
                    $ProviderService->service_type_id = $value;
		            $ProviderService->provider_id = Auth::user()->id;
                    $ProviderService->service_number = $request->service_number;
                    $ProviderService->service_model = $request->service_model;
                    $ProviderService->service_make = $request->service_make;
                    $ProviderService->service_year = $request->service_year;
                    $ProviderService->engine_standard=$request->engine_standard;
                    $ProviderService->save();
                }
            }
            else{
                foreach (explode(',', $request->service_type) as  $value) {
                    $ProviderService = new ProviderService;
                    $ProviderService->service_type_id = $value;
		            $ProviderService->provider_id = Auth::user()->id;
                    $ProviderService->service_number = $request->service_number;
                    $ProviderService->service_model = $request->service_model;
                    $ProviderService->service_make = $request->service_make;
                    $ProviderService->service_year = $request->service_year;
                    $ProviderService->engine_standard=$request->engine_standard;
                    $ProviderService->save();
                }
            }


            $Provider->save();

            return $Provider;
        }

        catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Provider Not Found!'], 404);
        }
    }

    /**
     * Update latitude and longitude of the user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function location(Request $request)
    {
        $this->validate($request, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
            ]);

        if($Provider = Auth::user()){

            $Provider->latitude = $request->latitude;
            $Provider->longitude = $request->longitude;
            $Provider->save();

            return response()->json(['message' => 'Location Updated successfully!']);

        } else {
            return response()->json(['error' => 'Provider Not Found!']);
        }
    }

    /**
     * Toggle service availability of the provider.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function available(Request $request)
    {
        $this->validate($request, [
                'service_status' => 'required|in:active,offline',
            ]);

        $Provider = Auth::user();
        
        if($Provider->service) {
            $Provider->service->update(['status' => $request->service_status]);
        } else {
            return response()->json(['error' => 'You account has not been approved for driving']);
        }

        return $Provider;
    }

    /**
     * Update password of the provider.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function password(Request $request)
    {
        $this->validate($request, [
                'password' => 'required|confirmed',
                'password_old' => 'required',
            ]);

        $Provider = Auth::user();

        if(password_verify($request->password_old, $Provider->password))
        {
            $Provider->password = bcrypt($request->password);
            $Provider->save();

            return response()->json(['message' => 'Password changed successfully!']);
        } else {
            return response()->json(['error' => 'Please enter correct password'], 422);
        }
    }

    /**
     * Show providers daily target.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function target(Request $request)
    {
        try {
            
            $Rides = UserRequests::where('provider_id', Auth::user()->id)
                    ->where('status', 'COMPLETED')
                    ->where('created_at', '>=', Carbon::today())
                    ->with('payment', 'service_type')
                    ->get();

            return response()->json([
                    'rides' => $Rides,
                    'rides_count' => $Rides->count(),
                    'target' => Setting::get('daily_target','0')
                ]);

        } catch(Exception $e) {
            return response()->json(['error' => "Something Went Wrong"]);
        }
    }
    /**
     * Show providers car details.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_provider_car(Request $request)
    {
        $ProviderCar = ProviderService::with('service_type')
                ->where('provider_id',Auth::user()->id)
                ->first();

        return response()->json([
            'provider_car' => $ProviderCar,
        ]);
}
    /**
     * Show providers services.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_auto_service(Request $request)
    {
        $Provider = Auth::user();
        $auto_accept=$Provider->agree_auto_accept;
        
        $AllserviceList = DB::table('service_types')
            ->select('service_types.*',
                        DB::raw('if((select provider_services.provider_id from provider_services where service_types.id=provider_services.service_type_id AND provider_services.provider_id=' . Auth::user()->id . ')=' . Auth::user()->id . ',1,0) AS provider_service_status')
                    )
            ->get();
        return response()->json([
            'services' => $AllserviceList,
            'auto_accept' => $auto_accept,
        ]);

    }
    
    /**
     * Update providers services.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_auto_service(Request $request)
    {
        $this->validate($request, [
                'auto_accept' => 'numeric|in:1,0',
                'service_number' => 'required',
                'service_model' => 'required',
                'service_make' => 'required',
                'service_year' => 'sometimes|year'
            ]);
        
        if ($request->has('auto_accept')) {
            Provider::where('id',Auth::user()->id)->update(['agree_auto_accept' => $request->auto_accept]);
        }
        $Provider = Auth::user();

        if($request->has('service_type')) {
            // if($Provider->service) {
            //     foreach ($request->service_type as  $service) {
            //         $ProviderService = ProviderService::where('provider_id',Auth::user()->id)
            //             ->update([
            //                 "service_type_id" => $service['services_id'],
            //                 "service_number" => $request->service_number,
            //                 "service_model" => $request->service_model,
            //             ]);
            //     }
            // }
            if($Provider->service) {
                $ProviderService = ProviderService::where('provider_id', Auth::user()->id)->delete();
                foreach ($request->service_type as  $service) {
                    if ($service['activated'] == '1') {
                        $ProviderService = new ProviderService;
                        $ProviderService->service_type_id = $service['services_id'];
                        $ProviderService->provider_id = Auth::user()->id;
                        $ProviderService->service_number = $request->service_number;
                        $ProviderService->service_model = $request->service_model;
                        $ProviderService->service_year = $request->service_year;
                        $ProviderService->service_make = $request->service_make;
                        $ProviderService->save();
                    }
                }
            }
        }
        return response()->json([
            'done' => "Preference has been changed",
        ]);
    }

    /**
     * Get activated Promotion.
     *
     * @return \Illuminate\Http\Response
     */

    public function getPromotion(Request $request)
    {
        $Promocode = $this->check_expiry();

        if($Promocode){
            $PromotionUsages = PromotionUsages::where('promocode_id', $Promocode->id)
                ->where('provider_id', Auth::user()->id)
                ->first();
            
            $usageId = ($PromotionUsages) ? $PromotionUsages->promocode_id : 0 ;

            if ($usageId > '0') {
                return response()->json([
                    'promo_found' => 1,
                    'promocode' => $Promocode,
                    'in_use' => 1
                    ]); 
            }else{
                return response()->json([
                    'promo_found' => 1,
                    'promocode' => $Promocode,
                    'in_use' => 0
                    ]); 
            }
        }else{
            PromotionUsages::where('provider_id', Auth::user()->id)
            ->update(['status' => 'EXPIRED']);
            return response()->json([
                'promo_found' => 0
                ]); 
        }

        
    }

    /**
     * Update activated Promotion for driver.
     *
     * @return \Illuminate\Http\Response
     */

    public function updatePromotion(Request $request)
    {
        $find_promo = Promocode::where('id',$request->promotion_id)->first();
        $Promocode = $this->check_expiry();
        $PromotionUsages = PromotionUsages::where('promocode_id', $Promocode->id)
                ->where('provider_id', Auth::user()->id)
                ->first();
        $usageId = ($PromotionUsages) ? $PromotionUsages->promocode_id : 0 ;
        if($request->promotion_id == $Promocode->id){
            if($usageId != $request->promotion_id){
                $promo = new PromotionUsages;
                    $promo->promocode_id = $find_promo->id;
                    $promo->provider_id = Auth::user()->id;
                    $promo->duration = 0;
                    $promo->distance = 0;
                    $promo->status = 'ADDED';
                    $promo->save();
                return response()->json([
                    'message' => "Promotion activated"
                    ]); 
            }else{
                return response()->json([
                    'message' => "Promotion already in use"
                    ]); 
            }
        }else{
            return response()->json([
                'message' => "Invalid Promotion id"
                ]); 
        }
    }

    public function check_expiry(){
        try{
            $Promocode = Promocode::where('user_type', 'DRIVER')
                ->where('activated_fordriver', '1')
                ->where('status', 'ADDED')
                ->first();

            if($Promocode != null){
                if(date("Y-m-d") > $Promocode->expiration){
                    $Promocode->status = 'EXPIRED';
                    $Promocode->save();
                    PromotionUsages::where('promocode_id', $Promocode->id)->update(['status' => 'EXPIRED']);
                }else{
                    PromotionUsages::where('promocode_id', $Promocode->id)->update(['status' => 'ADDED']);
                }
                return $Promocode;
            }else{
                return false;
            }
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }
}
