<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function requestValidation($request,$rule){
        $validator =  Validator::make($request,$rule);
        if ($validator->fails()) {
            $array=$validator->getMessageBag()->toArray();
            foreach($array as $key=>$val){
                return (object)['message'=>$val[0],"field"=>$key,"status"=>"false"];
            }
        }
        return (object)['message'=>null,"field"=>null,"status"=>"true"];
    }
    
}
