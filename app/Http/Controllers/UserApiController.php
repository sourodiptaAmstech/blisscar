<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;
use Log;
use Auth;
use Hash;
use Storage;
use Setting;
use Exception;
//use Notification;

use Carbon\Carbon;
use App\Http\Controllers\SendPushNotification;
use App\Notifications\ResetPasswordOTP;
use App\Notifications\ConfirmBooking;

use App\Helpers\Helper;

use App\Card;
use App\Account;
use App\User;
use App\Provider;
use App\Settings;
use App\Promocode;
use App\Locationfee;
use App\ServiceType;
use App\UserRequests;
use App\AirportUserRequests;
use App\NewsAndEvents;
use App\DriverTowardsDestination;
use App\RequestFilter;
use App\PromocodeUsage;
use App\ProviderService;
use App\UserRequestRating;
use App\DriverAirportQueue;
use App\Http\Controllers\ProviderResources\TripController;
use App\ProviderDocument;
use App\MultipleWayPoints;


use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
 
use Illuminate\Support\Facades\Notification;
use App\Services\TwilioSMS;

class UserApiController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function signup(Request $request)
    {
        $this->validate($request, [
                'social_unique_id' => ['required_if:login_by,facebook,google','unique:users'],
                'device_type' => 'required|in:android,ios',
                'device_token' => 'required',
                'device_id' => 'required',
                'login_by' => 'required|in:manual,facebook,google',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'mobile' => 'required',
                'password' => 'required|min:6',
            ]);

        try{
            
            $User = $request->all();

            $User['payment_mode'] = 'CARD';
            $User['password'] = bcrypt($request->password);
            $User = User::create($User);

            return $User;
        } catch (Exception $e) {
             return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function logout(Request $request)
    {
        try {
            User::where('id', $request->id)->update(['device_id'=> '', 'device_token' => '']);
            return response()->json(['message' => trans('api.logout_success')]);
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function change_password(Request $request){

        $this->validate($request, [
                'password' => 'required|confirmed|min:6',
                'old_password' => 'required',
            ]);

        $User = Auth::user();

        if(Hash::check($request->old_password, $User->password))
        {
            $User->password = bcrypt($request->password);
            $User->save();

            if($request->ajax()) {
                return response()->json(['message' => trans('api.user.password_updated')]);
            }else{
                return back()->with('flash_success', 'Password Updated');
            }

        } else {
            return response()->json(['error' => trans('api.user.incorrect_password')], 500);
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function update_location(Request $request){

        $this->validate($request, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
            ]);

        if($user = User::find(Auth::user()->id)){

            $user->latitude = $request->latitude;
            $user->longitude = $request->longitude;
            $user->save();

            return response()->json(['message' => trans('api.user.location_updated')]);

        }else{

            return response()->json(['error' => trans('api.user.user_not_found')], 500);

        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function details(Request $request){
 
        $this->validate($request, [
            'device_type' => 'in:android,ios',
        ]);

        try{

            if($user = User::find(Auth::user()->id)){

                if($request->has('device_token')){
                    $user->device_token = $request->device_token;
                }

                if($request->has('device_type')){
                    $user->device_type = $request->device_type;
                }

                if($request->has('device_id')){
                    $user->device_id = $request->device_id;
                }

                $user->save();

                $user->currency = Setting::get('currency');
                $user->sos = Setting::get('sos_number', '911');
                return $user;

            } else {
                return response()->json(['error' => trans('api.user.user_not_found')], 500);
            }
        }
        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function update_profile(Request $request)
    {

        $this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'max:255',
                'email' => 'email|unique:users,email,'.Auth::user()->id,
                'mobile' => 'required',
                'isdCode'=>'required',
                'picture' => 'mimes:jpeg,bmp,png',
            ]);

         try {

            $user = User::findOrFail(Auth::user()->id);

            if($request->has('first_name')){ 
                $user->first_name = $request->first_name;
            }
            
            if($request->has('last_name')){
                $user->last_name = $request->last_name;
            }
            
            if($request->has('email')){
                $user->email = $request->email;
            }
        
            if($request->has('mobile')){
                $user->mobile = $request->mobile;
            }

            if($request->has('isdCode')){
                $user->isdCode = $request->isdCode;
            }

            if ($request->picture != "") {
                Storage::delete($user->picture);
                $user->picture = $request->picture->store('app/public/user/profile');
                $user->picture=str_replace("app/public/", "", $user->picture);
            }

            $user->save();

            if($request->ajax()) {
                return response()->json($user);
            }else{
                return back()->with('flash_success', trans('api.user.profile_updated'));
            }
        }

        catch (ModelNotFoundException $e) {
             return response()->json(['error' => trans('api.user.user_not_found')], 500);
        }

    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function services(Request $request) {
		
        $serviceList = ServiceType::all();

       // echo env('GOOGLE_MAP_KEY');die;

       //return $abc = env('GOOGLE_MAP_KEY');


 		
            $estimated_fare_details=array();
            $estimated_distance=array();
        foreach ($serviceList as $service) {
            
            $request->request->add(['service_type' => $service->id]);         

			
            $datate=$this->estimated_fare($request)->getData();

            
		
            if (property_exists($datate, 'estimated_fare')) 
            {

               $verr= $datate->estimated_fare;
           
            $estimated_fare_details[] = array(
                'id' => $service->id,
                // 'estimated_fare' => $this->estimated_fare($request)->getData()->estimated_fare,
                'estimated_fare' =>  $verr,
            );
            }
            
            if (property_exists($datate, 'sourceToDestinationDistance')) 
            {

               $sourceToDestinationDistance= $datate->sourceToDestinationDistance;
           
            $estimated_distance[] = array(
                'id' => $service->id,
                // 'estimated_fare' => $this->estimated_fare($request)->getData()->estimated_fare,
                'sourceToDestinationDistance' =>  $sourceToDestinationDistance,
            );
            }
        }
        

 

//print_r($estimated_fare_details);die;
        if (count($estimated_fare_details)<=0) 
            	return response()->json(['message' => "No driver available in your location at this moment."],500);

		 // die();
        $output = array();



        // $arrayAB = array_merge($serviceList->toBase()->toArray(), $estimated_fare_details);
        $arrayAB = array_merge($estimated_fare_details, $serviceList->toBase()->toArray(),$estimated_distance);
        foreach ( $arrayAB as $value ) {
        $id = $value['id'];
        if ( !isset($output[$id]) ) {
            $output[$id] = array();
        }
        $output[$id] = array_merge($output[$id], $value);
        }
        // echo "<br>";
        //  print_r($output) ;

         $finalarry=array();

         	foreach ($output as $key ) {
         		if( isset($key['estimated_fare']))
				$finalarry[]=$key;
         	}


         // echo "<br>";
         // print_r($finalarry) ;


        $emptycollectionofservice = collect(new ServiceType);
        if($serviceList) {
            $return=[];
         $return["response"]=$emptycollectionofservice->toBase()->merge($finalarry)->unique();
        // print_r($return[0]); exit;
            return $return;
        } else {
            return response()->json(['error' => trans('api.services_not_found')], 500);
        }

    }

  public function news_events(Request $request) {
       if($request->has('app'))
		   {
            // news_type =0 rider 
            // news_type =1 Driver 
            $newsandevents = NewsAndEvents::where('news_type','0')->orWhere('news_type','2')->orderBy('created_at' , 'desc')->get();  

            return response()->json([
                'newsandevents' => $newsandevents,
            ]);
        }   
     }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function send_request(Request $request) {
        
     //  print_r($request->way_point); exit;
    
        
    //     $this->validate($request, [
    //             's_latitude' => 'required|numeric',
    //             'd_latitude' => 'required|numeric',
    //             's_longitude' => 'required|numeric',
    //             'd_longitude' => 'required|numeric',
    //             'service_type' => 'required|numeric|exists:service_types,id',
    //             'promo_code' => 'exists:promocodes,promo_code',
    //             'distance' => 'required|numeric',
    //             'use_wallet' => 'numeric',
    //             'payment_mode' => 'required|in:CASH,CARD,PAYPAL',
    //             'card_id' => ['required_if:payment_mode,CARD','exists:cards,card_id,user_id,'.Auth::user()->id],
    //         ]);

    //     Log::info('New Request from User: '.Auth::user()->id);
    //    Log::info('Request Details:', $request->all());

	
        $ActiveRequests = UserRequests::PendingRequest(Auth::user()->id)->count();
      
	
			
		
        if($ActiveRequests > 0) {
            return response()->json(['error' => trans('api.ride.request_inprogress')], 500);
            if($request->ajax()) {
                return response()->json(['error' => trans('api.ride.request_inprogress')], 500);
            } else {
                return redirect('dashboard')->with('flash_error', 'Already request is in progress. Try again later');
            }
        }
 	//echo 419; exit;
        if($request->has('schedule_date') && $request->has('schedule_time')){
            $beforeschedule_time = (new Carbon("$request->schedule_date $request->schedule_time"))->subHour(1);
            $afterschedule_time = (new Carbon("$request->schedule_date $request->schedule_time"))->addHour(1);

            $CheckScheduling = UserRequests::where('status','SCHEDULED')
                            ->where('user_id', Auth::user()->id)
                            ->whereBetween('schedule_at',[$beforeschedule_time,$afterschedule_time])
                            ->count();


            if($CheckScheduling > 0){
                return response()->json(['error' => "You have alreday set a shedule ride within this hour, please try another time"], 500);
                if($request->ajax()) {
                    //return response()->json(['error' => trans('api.ride.request_scheduled')], 500);
                    return response()->json(['error' => "You have alreday set a shedule ride within this hour, please try another time"], 500);
                }else{
                    return redirect('dashboard')->with('flash_error', 'Already request is Scheduled on this time.');
                }
            }

        }

        $distance = Setting::get('provider_search_radius', '10');
        $distnace=$distance*1.609;
        $latitude = $request->s_latitude;
        $longitude = $request->s_longitude;
        $service_type = $request->service_type;

		
		
		
        /*//If user in Airport
        $locations = LocationFee::orderBy('created_at' , 'desc')->get()->toArray();
        foreach ($locations as $value) {
            $loc_details[] = array(
                'kmd' => $this->getDistance($latitude, $longitude, $value['latitude'], $value['longitude']),
                'location_id' =>$value['id']
            );
        }
        usort($loc_details, array('App\Http\Controllers\UserApiController','sortByOrder'));

        if($loc_details[0]['kmd'] * 1000 <= 1000)
        {
            $ProviderQueue = DriverAirportQueue::Queue()
                ->where('location_id', $loc_details[0]['location_id'])
                ->get();
            if(count($ProviderQueue) > 0){
        $Providers = Provider::with('service')
                    ->where('id', $ProviderQueue[0]->provider_id)
                    ->get();
            }else{
                $Providers = [];
            }
        }else{*/
        
         
			
				


// ****************************************************** SELECTED DESTINATION DRIVER WHO SETED THEIR TWOWARDS DESTINATION WITH DESTINATION FROM 'PROVIDER' AND "TOWARDS DESTINATION" *****
         
        $dlatitude = $request->d_latitude;
        $dlongitude = $request->d_longitude;
       
        
 $DestinationSetProviders = Provider::with('service') ->where('status', 'approved') ->join('driver_towards_destinations', 'providers.id', '=', 'driver_towards_destinations.provider_id')
			  ->select(DB::Raw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'providers.id')
            ->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
            ->whereHas('service', function($query) use ($service_type){
                        $query->where('status','active');
                        $query->where('service_type_id',$service_type);
                    })
			    ->select(DB::Raw("(6371 * acos( cos( radians('$dlatitude') ) * cos( radians(driver_towards_destinations.d_lat) ) * cos( radians(driver_towards_destinations.d_lng) - radians('$dlongitude') ) + sin( radians('$dlatitude') ) * sin( radians(driver_towards_destinations.d_lat) ) ) ) AS distance"),'providers.id','driver_towards_destinations.provider_id')
        		 ->whereRaw("(6371 * acos( cos( radians('$dlatitude') ) * cos( radians(driver_towards_destinations.d_lat) ) * cos( radians(driver_towards_destinations.d_lng) - radians('$dlongitude') ) + sin( radians('$dlatitude') ) * sin( radians(driver_towards_destinations.d_lat) ) ) ) <= 2")
            ->orderBy('distance')
            ->get();

//print_r($DestinationSetProviders);
      // return response()->json(['Towards'=>'yes','message' => $Providers]); 
         	// die();

// *******************************************************************************************************************************************************************************



// ****************************************************** SELECTED DESTINATION DRIVER WHO NOT THEIR TWOWARDS DESTINATION WITH DESTINATION FROM 'PROVIDER' AND "TOWARDS DESTINATION" *****
         
       

        
 $GeneralProviders = Provider::with('service') ->where('status', 'approved')

 			->leftJoin('driver_towards_destinations', 'providers.id', '=', 'driver_towards_destinations.provider_id')
 			->whereNull('driver_towards_destinations.provider_id')
			->select(DB::Raw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'providers.id')
			->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
           ->whereHas('service', function($query) use ($service_type){
                        $query->where('status','active');
                        $query->where('service_type_id',$service_type);
                    })
			->orderBy('distance')
			   ->get();



// *******************************************************************************************************************************************************************************


// **************************************** MARGE TOW TYPE PROVIDER ***************************************************************
			    $Providers  = array();
				 foreach ($DestinationSetProviders as $key => $Provider1) {
				 $Providers[]=$Provider1;
				 }

				foreach ($GeneralProviders as $key => $Provider1) {
				 	 $Providers[]=$Provider1;
				 }			 
				
               

				 	/********************************** AIRPORT INSTRUCTION NOTIFICATION *****************************************************************/

						$rediusDistance=3.21;

 										$Sourcelocations = LocationFee::select('*')
										           	->select(DB::Raw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'locationfees.id')
										           ->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $rediusDistance")
										           ->orderBy('distance')
										           ->get();
                                               
										           if( $Sourcelocations->count() >0){	
 																	$airportInstructionPushsData = DB::table('airport_instruction_pushs')->get();	
																		 // echo $airportInstructionPushsData;			
														                if(!empty($airportInstructionPushsData))
																		{
																			
																			$message = \PushNotification::Message($airportInstructionPushsData[0]->title,array(
																			'badge' => 1,
																			'sound' => 'default',
																			'actionLocKey' => 'Blisscars247 Ride',
																			'locKey' => 'localized key',
																			'locArgs' => array(
																				'localized args',
																				'localized args',
																			),
																			'launchImage' => $airportInstructionPushsData[0]->description,
																			'description' => $airportInstructionPushsData[0]->description,
																			'custom' => array('custom_data' => array('blisscars247' => 'AirportInstruction', 'Blisscars247News'))
														                   )); 

														                  
														                     (new SendPushNotification)->sendPushToUser(Auth::user()->id,$message);	
																		}		
										           }

                                                 
				
				/************************************************** AIRPORT REQUEST ORDERING AND CHEKING ******************************************************/
				
		// if user request for Airport then send airport instructions push as well as make priority for accept this request
														
												try{
										           $locations = LocationFee::select('*')
										           	->select(DB::Raw("(6371 * acos( cos( radians('$dlatitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$dlongitude') ) + sin( radians('$dlatitude') ) * sin( radians(latitude) ) ) ) AS distance"),'locationfees.id')
										           ->whereRaw("(6371 * acos( cos( radians('$dlatitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$dlongitude') ) + sin( radians('$dlatitude') ) * sin( radians(latitude) ) ) ) <= $rediusDistance")
										           ->orderBy('distance')
										           ->get();

												
										          

														            if( $locations->count()>0){																				
																		 $driverAirportQueueArray = DriverAirportQueue::select('provider_id')->where('location_id', $locations[0]['id'])->orderBy('updated_at', 'DSC')->get();			 
																		 
																		 if(count($driverAirportQueueArray)>0)
																		 {					
																			 $driverAirportQueueNormalArray=json_decode($driverAirportQueueArray, true);
																			 
																			 /*print_r($Providers);
																			 echo "================";
																			 print_r($driverAirportQueueNormalArray);*/
																			 $tempProviders  = array();

																			 	
																				 	 foreach ($driverAirportQueueNormalArray as $key => $Provider2) {
																				 	 		foreach ($Providers as $key => $Provider1) {
																			 					$index1=$key;
																				 	 			
																				 				if($Provider1['id']===$Provider2['provider_id'])
																				 				{
																				 					array_unshift($tempProviders , $Provider1);
																				 					  unset($Providers[$index1]);

																				 				}
																				 				
																				 				}	
																				 	}	

																				 $Providers=array_merge($tempProviders,$Providers);
																				 // print_r(array_merge($tempProviders,$Providers));

																			  // return response()->json(['error' => $Providers, "data"=>$tempProviders,'airport'=>$driverAirportQueueNormalArray]);

																			
														                     
																		 }
																		 			
																	} 


										 
												}catch(Exception $e)
												{  
													echo $e;
													die();
												}
                                               
					/************************************************** END AIRPORT REQUEST ORDERING AND CHEKING ******************************************************/
		
			
				 
// *****************************************************************************************************************************



        // $Providers = Provider::with('service')
        //     ->select(DB::Raw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'id')
        //     ->where('status', 'approved')
        //     ->whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
        //     ->whereHas('service', function($query) use ($service_type){
        //                 $query->where('status','active');
        //                 $query->where('service_type_id',$service_type);
        //             })
        //     ->orderBy('distance')
        //     ->get();
        
         // return response()->json(['Towards'=>'yes','message' => $Providers]); 
        	//  die();




        /* Towards Destination Serach by Request Locattion Destination *******************/
        // $dlatitude = $request->d_latitude;
        // $dlongitude = $request->d_longitude;

      //  $oldexist = DriverTowardsDestination::select('id','d_lat','d_lng')->get();

         // $TowardsActiveProviders = DriverTowardsDestination::select(DB::Raw("(6371 * acos( cos( radians('$dlatitude') ) * cos( radians(d_lat) ) * cos( radians(d_lng) - radians('$dlongitude') ) + sin( radians('$dlatitude') ) * sin( radians(d_lat) ) ) ) AS distance"),'id','provider_id')
         // ->whereRaw("(6371 * acos( cos( radians('$dlatitude') ) * cos( radians(d_lat) ) * cos( radians(d_lng) - radians('$dlongitude') ) + sin( radians('$dlatitude') ) * sin( radians(d_lat) ) ) ) <= 2")
         //  ->orderBy('distance')
         // ->get();

        	
        
  //       $WaytoProvider=DB::select( DB::raw("SELECT
  //                                           id,provider_id
  //                                       FROM
  //                                           (
  //                                           SELECT
  //                                               id,
  //                                               provider_id,
  //                                               ASTEXT(poligons),
  //                                               units * DEGREES(
  //                                                   ACOS(
  //                                                       COS(RADIANS(latpoint)) * COS(RADIANS(X(poligons))) * COS(
  //                                                           RADIANS(longpoint) - RADIANS(Y(poligons))
  //                                                       ) + SIN(RADIANS(latpoint)) * SIN(RADIANS(X(poligons)))
  //                                                   )
  //                                               ) AS distance
  //                                           FROM
  //                                               driver_towards_destinations
  //                                           JOIN(
  //                                               SELECT $dlatitude AS latpoint,
  //                                                   $dlongitude AS longpoint,
  //                                                   6.0 AS r,
  //                                                   69.0 AS units
  //                                           ) AS p
  //                                       ON
  //                                           (1 = 1)
  //                                       WHERE
  //                                           MBRCONTAINS(
  //                                               GEOMFROMTEXT(
  //                                                   CONCAT(
  //                                                       'LINESTRING(',
  //                                                       latpoint -(r / units),
  //                                                       ' ',
  //                                                       longpoint -(
  //                                                           r /(units * COS(RADIANS(latpoint)))
  //                                                       ),
  //                                                       ',',
  //                                                       latpoint +(r / units),
  //                                                       ' ',
  //                                                       longpoint +(
  //                                                           r /(units * COS(RADIANS(latpoint)))
  //                                                       ),
  //                                                       ')'
  //                                                   )
  //                                               ),
  //                                               poligons
  //                                           )
  //                                       ) AS d
  //                                       ORDER BY
  //                                           d.distance ASC"));
        
  //       $first_Priority_provider = array();

		//  foreach ($Providers as $key => $Provider1) {
		// 	//echo($Provider1->id.'<- Driver');
  //                foreach ($TowardsActiveProviders as $key => $Provider) {
  //                   //echo($Provider->provider_id.'<- Towards>');

  //                       if($Provider1->id == $Provider->provider_id)
  //                       {
  //                           $first_Priority_provider[]=$Provider1;
  //                       }
  //               }
		// }

	// return response()->json(['Towards'=>'yes','message' => $Providers,"Towars"=>$TowardsActiveProviders
 //       		 				,"Final"=>$first_Priority_provider]); 
 //        	 die();
        // List Providers who are currently busy and add them to the filter list.

        if(count($Providers) == 0) {
            return response()->json(['message' => trans('api.ride.no_providers_found')]);
            if($request->ajax()) {
                // Push Notification to User
                return response()->json(['message' => trans('api.ride.no_providers_found')]); 
            }else{
                return back()->with('flash_success', 'No Providers Found! Please try again.');
            }
        }
        //}

        $isInCongesionZone=false; 
        $isLezzone=false;

        try{
            $wayPointLatLog="";
            if($request->has('way_point')){
             $waypointArray=json_decode($request->way_point,true);
           
           usort($waypointArray, function($a, $b) {return  strcmp($a['order'], $b['order']);});
            $wayPointLatLogArray=$waypointArray;
             if(count($waypointArray)>0){
                 foreach($waypointArray as $key=>$val){
                     if($wayPointLatLog===""){
                         $wayPointLatLog="&waypoints=".$val['lat'].','.$val['lng'];
                        }
                        else{
                            $wayPointLatLog=$wayPointLatLog."|".$val['lat'].','.$val['lng'];
                        }

                      




                    }
             }
             
            }

         //  $isInCongesionZone=   $this->checkCongessions(array("$request->s_latitude $request->s_longitude"));

           // return response()->json(['error' => $isInCongesionZone], 200);



            if($wayPointLatLog===""){
                $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$request->s_latitude.",".$request->s_longitude."&destination=".$request->d_latitude.",".$request->d_longitude.$wayPointLatLog."&mode=driving&key=".env('GOOGLE_MAP_KEY');
            }            
            else{
                $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$request->s_latitude.",".$request->s_longitude."&destination=".$request->d_latitude.",".$request->d_longitude."&mode=driving&key=".env('GOOGLE_MAP_KEY');
            }
           

            $json = curl($details);

            $details = json_decode($json, TRUE);

           

            $route_key = $details['routes'][0]['overview_polyline']['points'];
 //echo "<pre>";
        //    print_r($details); exit;
            $UserRequest = new UserRequests;
            $UserRequest->booking_id = Helper::generate_booking_id();
            $UserRequest->user_id = Auth::user()->id;
            $UserRequest->current_provider_id = $Providers[0]->id;
            $UserRequest->service_type_id = $request->service_type;
            $UserRequest->payment_mode = $request->payment_mode;
            
            $UserRequest->status = 'SEARCHING';

            $UserRequest->s_address = $request->s_address ? : "";
            $UserRequest->d_address = $request->d_address ? : "";

            $UserRequest->s_latitude = $request->s_latitude;
            $UserRequest->s_longitude = $request->s_longitude;

            $UserRequest->d_latitude = $request->d_latitude;
            $UserRequest->d_longitude = $request->d_longitude;
            $UserRequest->distance = $request->distance;
            $UserRequest->congestionCharge=$request->congestionCharge;
            $UserRequest->cleanAirCharges=$request->cleanAirCharges;

            if(Auth::user()->wallet_balance > 0){
                $UserRequest->use_wallet = $request->use_wallet ? : 0;
            }

            $UserRequest->assigned_at = Carbon::now();
            $UserRequest->route_key = $route_key;

           /* if($finalprovider->count() <= Setting::get('surge_trigger') && $finalprovider->count() > 0){
                $UserRequest->surge = 1;
            } */
            
            if(count($Providers) <= Setting::get('surge_trigger') && count($Providers) > 0){
                $UserRequest->surge = 1;
            }

            if($request->has('schedule_date') && $request->has('schedule_time')){
                $UserRequest->schedule_at = date("Y-m-d H:i:s",strtotime("$request->schedule_date $request->schedule_time"));
                $UserRequest->flight_info = $request->flight_info;
                $UserRequest->gratuity = $request->gratuity;
                $UserRequest->comment_spinfo = $request->comment_spinfo;
                $UserRequest->status = 'SEARCHING';
            }

            if($request->has('schedule_date') && $request->has('schedule_time')){
                $UserRequest->ride_type = 'SCHEDULED_RIDE';
            }                      
            else            
            {
                $UserRequest->ride_type = 'RIDE_NOW';
            }
            $UserRequest->multiple_waypoints ='SINGLE';
            if($wayPointLatLog!==""){
                $UserRequest->multiple_waypoints ='MULTIPLE_WAYPOINTS';
            }  


            //dd($UserRequest);
            //print_r($UserRequest);exit;
           $UserRequest->save();
            if($UserRequest->multiple_waypoints==='MULTIPLE_WAYPOINTS'){               
                  if(count($wayPointLatLogArray)>0){
                 foreach($wayPointLatLogArray as $key=>$val){
                    $MULTIPLEWAYPOINTS=new MultipleWayPoints();
                    $MULTIPLEWAYPOINTS->user_requests_id=$UserRequest->id;
                    $MULTIPLEWAYPOINTS->w_address=$val["location_name"];
                    $MULTIPLEWAYPOINTS->w_latitude=$val['lat'];
                    $MULTIPLEWAYPOINTS->w_longitude=$val['lng'];
                    $MULTIPLEWAYPOINTS->status='0'; 
                    $MULTIPLEWAYPOINTS->order=$val['order'];
                    $MULTIPLEWAYPOINTS->save();
                   }
                }
            }
            
            Log::info('New Request id : '. $UserRequest->id .' Assigned to provider : '. $UserRequest->current_provider_id);
            Log::info('New Ride Request : '. $UserRequest);


            // update payment mode 

            User::where('id',Auth::user()->id)->update(['payment_mode' => $request->payment_mode]);

            if($request->has('card_id')){

                Card::where('user_id',Auth::user()->id)->update(['is_default' => 0]);
                Card::where('card_id',$request->card_id)->update(['is_default' => 1]);
            }

           
			
			
			/* ******************** FOR PUSH SEND FIRST PROVIDER **************************/
			
			 if(count($Providers) > 0)
			 {
				  (new SendPushNotification)->IncomingRequest($Providers[0]->id);
			 }
			
			
			

            foreach ($Providers as $key => $Provider) {

                $Filter = new RequestFilter;
                // Send push notifications to the first provider
                // incoming request push to provider
                 // (new SendPushNotification)->IncomingRequest($Provider->id);
                $Filter->request_id = $UserRequest->id;
                $Filter->provider_id = $Provider->id; 
                $Filter->save();
            }

            //Auto accept
            /*$Current_providerService = ProviderService::where('provider_id', $Providers[0]->id)->get();
            $Current_provider_service_type = $Current_providerService->toArray()[0]['service_type_id'];
            $UserDetails = User::where('id', Auth::user()->id)->get();
            $UserRating = $UserDetails->toArray()[0]['rating'];*/
          
            $UserRating = Auth::user()->rating;
            $Current_rider_service_type = $request->service_type;

            $Current_providerDetails = Provider::with('service')
                        ->where('id', $Providers[0]->id)
                        ->where('status', 'approved')
                        ->whereHas('service', function($query) use ($Current_rider_service_type){
                            $query->where('status','active');
                            $query->where('service_type_id',$Current_rider_service_type);
                        })
                        ->get();
            $Current_provider_latitude = $Current_providerDetails->toArray()[0]['latitude'];
            $Current_provider_longitude = $Current_providerDetails->toArray()[0]['longitude'];
            
            $details_new = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$Current_provider_latitude.",".$Current_provider_longitude."&destinations=".$request->s_latitude.",".$request->s_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');
            $json_new = curl($details_new);
            $details_new = json_decode($json_new, TRUE);
           
            $est_seconds = $details_new['rows'][0]['elements'][0]['duration']['value'];
//dd($UserRequest);
            //if(false){
            if($UserRating >= "4.7" && $est_seconds <= "300"){
                $allReqFilter = RequestFilter::where('request_id', $UserRequest->id)
                                ->join('providers', 'request_filters.provider_id', '=', 'providers.id')
                                //->where('providers.updated_at', '<', Carbon::now()->subMinutes(15)->toDateTimeString())
                                ->where('agree_auto_accept', '=', 1)
                                ->select('request_filters.*', 'providers.agree_auto_accept')
                                ->get();
//dd($allReqFilter);
                if($allReqFilter->isNotEmpty()){

                    $UserRequest->auto_accepted_provider_id = $allReqFilter->toArray()[0]['provider_id'];
                    $UserRequest->auto_accepted = '1';
                    $UserRequest->save();
//dd($UserRequest);

                    $autoaccepted = (new TripController())->autoaccept($request, $UserRequest->id);
                    
                    if($autoaccepted){
                        return response()->json([
                            'message' => 'Ride Started!',
                            'request_id' => $UserRequest->id,
                            'current_provider' => $UserRequest->auto_accepted_provider_id,
                        ]);
                    }
                }
            }
            //Auto accept end
//dd($UserRequest);
            if($request->ajax()) {
                return response()->json([
                        'message' => 'New request Created!',
                        'request_id' => $UserRequest->id,
                        'current_provider' => $UserRequest->current_provider_id,
                    ]);
            }else{
                return redirect('dashboard');
            }

        } catch (Exception $e) {
			echo $e;
			die();
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something went wrong while sending request. Please try again.' +$e);
            }
        }
    }


    /// AIRPORT TRANSFER


    public function airportTransfer(Request $request){
    /*    $this->validate($request, [
            's_latitude'=>'required|numeric',
            'd_latitude'=>'required|numeric',
            's_longitude'=>'required|numeric',
            'd_longitude'=>'required|numeric',
            'service_type'=>'required|numeric|exists:service_types,id',
            'pickUp_date'=>'required',
            'pickup_time'=>'required',
            'flight_details'=>'required',
            'pick_up_address'=>'required',
            'drop_addresss'=>'required',
            'no_of_passenger'=>'required',
            'hand_luggage'=>'required',
            'suitcase_luggage'=>'required',
            'comment'=>'required',
          //  'promo_code' => 'exists:promocodes,promo_code',
            'payment_mode'=>'required|in:CASH,CARD'
            
            ]);*/
            
            $ActiveRequests = UserRequests::PendingRequest(Auth::user()->id)->count();
            if($ActiveRequests > 0) {
                return response()->json(['error' => trans('api.ride.request_inprogress')], 500);
                if($request->ajax()) {
                    return response()->json(['error' => trans('api.ride.request_inprogress')], 500);
                }
                else{
                    return redirect('dashboard')->with('flash_error', 'Already request is in progress. Try again later');
                }
            }
            //echo 419; exit;
            if($request->has('schedule_date') && $request->has('schedule_time')){
                $beforeschedule_time = (new Carbon("$request->schedule_date $request->schedule_time"))->subHour(1);
                $afterschedule_time = (new Carbon("$request->schedule_date $request->schedule_time"))->addHour(1);
                $CheckScheduling = UserRequests::where('status','SCHEDULED')
                ->where('user_id', Auth::user()->id)
                ->whereBetween('schedule_at',[$beforeschedule_time,$afterschedule_time])
                ->count();
                if($CheckScheduling > 0){
                    return response()->json(['error' => "You have alreday set a shedule ride within this hour, please try another time"], 500);
                    if($request->ajax()) {
                        //return response()->json(['error' => trans('api.ride.request_scheduled')], 500);
                        return response()->json(['error' => "You have alreday set a shedule ride within this hour, please try another time"], 500);
                    }else{
                        return redirect('dashboard')->with('flash_error', 'Already request is Scheduled on this time.');
                    }
                }
            }
            
            
            $distance = Setting::get('provider_search_radius', '10');
            $distnace=$distance*1.609;
            $latitude = $request->s_latitude;
            $longitude = $request->s_longitude;
            $service_type = $request->service_type;
            $dlatitude = $request->d_latitude;
            $dlongitude = $request->d_longitude;








            $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$request->s_latitude.",".$request->s_longitude."&destination=".$request->d_latitude.",".$request->d_longitude."&mode=driving&key=".env('GOOGLE_MAP_KEY');
            $json = curl($details);
            $details = json_decode($json, TRUE);
            $route_key = $details['routes'][0]['overview_polyline']['points'];


            $UserRequest = new AirportUserRequests;
            $UserRequest->booking_id = Helper::generate_booking_id();
            $UserRequest->user_id = Auth::user()->id;
            $UserRequest->current_provider_id = 0;
            $UserRequest->service_type_id = $request->service_type;
            $UserRequest->payment_mode = $request->payment_mode;
            
           // $UserRequest->status = 'SEARCHING';

            $UserRequest->s_address = $request->pick_up_address ? : "";
            $UserRequest->d_address = $request->drop_addresss ? : "";

            $UserRequest->s_latitude = $request->s_latitude;
            $UserRequest->s_longitude = $request->s_longitude;

            $UserRequest->d_latitude = $request->d_latitude;
            $UserRequest->d_longitude = $request->d_longitude;
            $UserRequest->distance = 0;//$request->distance;
            if(Auth::user()->wallet_balance > 0){
                $UserRequest->use_wallet = $request->use_wallet ? : 0;
            }

            $UserRequest->assigned_at = Carbon::now();
            $UserRequest->route_key = $route_key;
            $UserRequest->surge = 0;
            $UserRequest->schedule_at = date("Y-m-d H:i:s",strtotime("$request->pickUp_date $request->pickup_time"));
            $UserRequest->flight_info = $request->flight_details;
            $UserRequest->comment_spinfo = $request->comment;
            $UserRequest->suitcase_luggage=(int)$request->suitcase_luggage;
            $UserRequest->hand_luggage=(int)$request->hand_luggage;


            $UserRequest->status = 'AIRPORTREQUEST';
            $UserRequest->BookingType = 'AIRPORT';

            $UserRequest->ride_type = 'AIRPORT_REQUEST';
            $UserRequest->multiple_waypoints ='SINGLE';
            $UserRequest->no_of_passenger=$request->no_of_passenger;
            if($request->has('card_id')){

                Card::where('user_id',Auth::user()->id)->update(['is_default' => 0]);
                Card::where('card_id',$request->card_id)->update(['is_default' => 1]);
            }
            $UserRequest->congesionZoneCharge=$request->congesionZoneCharge;
            $UserRequest->cleanAirCharges=$request->cleanAirCharges;
            $UserRequest->airport_pickup_fee=$request->airport_pickup_fee;
            $UserRequest->airport_drop_fee=$request->airport_drop_fee;
//blisscars247@gmail.com

            $UserRequest->save();

            $user = Auth::user();
            $UserRequest->name=$user->last_name.' '.$user->first_name;
            $UserRequest->contact_number=$user->isdCode.'-'.$user->mobile;
            $UserRequest->email=$user->email;
            $UserRequest->luggage_capacity=$UserRequest->suitcase_luggage+$UserRequest->hand_luggage;
            $request->distnace=$distnace;
            $service_type=ServiceType::find($UserRequest->service_type_id);
            $UserRequest->service_name=$service_type->name;
            Notification::send($user, new ConfirmBooking($UserRequest,$request));

            $admin=Account::first();
            Notification::send($admin, new ConfirmBooking($UserRequest,$request));

            return response()->json([
                'message' => 'We have received your airport transfer request, we will get back to you soon.',
                'request_id' => $UserRequest->id,
                'current_provider' => $UserRequest->current_provider_id,
            ]);

       
        

    
    
    }

// check congession zone. blisscars247@gmail.com
private $pointOnVertex;
    public function checkCongessions($point){
        $polygon=array(
            "51.5183755 -0.1668667",
            "51.5205233 -0.1656066",
            "51.5213244 -0.1615522",
            "51.521637 -0.1596903",
            "51.5204942 -0.1591322",
            "51.5209488 -0.1568695",
            "51.5220474 -0.1574276",
            "51.5225904 -0.1543328",
            "51.5229061 -0.1526586",
            "51.5230639 -0.1511467",
            "51.5232154 -0.1490666",
            "51.5233669 -0.1479505",
            "51.5231522 -0.1477171",
            "51.5228428 -0.1469561",
            "51.5228175 -0.1462052",
            "51.5233226 -0.1451905",
            "51.5238719 -0.1449673",
            "51.5239666 -0.1443281",
            "51.5235878 -0.1442774",
            "51.5235436 -0.1437396",
            "51.5238593 -0.1430192",
            "51.5242081 -0.140694",
            "51.5249094 -0.1381976",
            "51.5237275 -0.1372406",
            "51.524579 -0.1350153",
            "51.5254675 -0.1357293",
            "51.5280588 -0.1285179",
            "51.5305167 -0.1222823",
            "51.5305167 -0.1222823",
            "51.5287696 -0.1196643",
            "51.5293915 -0.1155231",
            "51.5312867 -0.1137857",
            "51.5318789 -0.1064315",
            "51.5298357 -0.1025283",
            "51.5307093 -0.1016477",
            "51.5274223 -0.0887957",
            "51.5261341 -0.0880103",
            "51.5257195 -0.0866775",
            "51.5261341 -0.0840357",
            "51.5227727 -0.0778953",
            "51.5206787 -0.0748758",
            "51.5191806 -0.0746098",
            "51.5183198 -0.0745699",
            "51.5153565 -0.0720426",
            "51.514537 -0.0741443",
            "51.5115124 -0.0730532",
            "51.5113889 -0.0749873",
            "51.5102478 -0.0750337",
            "51.5092656 -0.0740589",
            "51.5079944 -0.074233",
            "51.5065788 -0.0748132",
            "51.5045708 -0.0761593",
            "51.502895 -0.0773197",
            "51.5014142 -0.0781668",
            "51.4992254 -0.0791764",
            "51.4978312 -0.0798843",
            "51.4968632 -0.0817758",
            "51.4960541 -0.0830175",
            "51.4950138 -0.0848278",
            "51.4950427 -0.0858954",
            "51.4945112 -0.0864823",
            "51.4943451 -0.0902436",
            "51.4943119 -0.0941383",
            "51.4943783 -0.0952587",
            "51.4949547 -0.09814",
            "51.4951096 -0.0991557",
            "51.4952473 -0.0997085",
            "51.4955527 -0.0998881",
            "51.4956818 -0.1002267",
            "51.4957734 -0.1010607",
            "51.4953458 -0.1014532",
            "51.4949837 -0.1012217",
            "51.4946673 -0.1008998",
            "51.4944106 -0.1007643",
            "51.4930458 -0.1008486",
            "51.4915104 -0.1029985",
            "51.4910905 -0.1051273",
            "51.4908412 -0.1061179",
            "51.4898963 -0.1082045",
            "51.4890695 -0.1107548",
            "51.4879277 -0.1119351",
            "51.4870877 -0.113537",
            "51.4869827 -0.1164246",
            "51.4865365 -0.1191436",
            "51.4862974 -0.1219729",
            "51.4865994 -0.1240443",
            "51.487216 -0.1258833",
            "51.4886631 -0.1291774",
            "51.4942471 -0.1405917",
            "51.4963523 -0.1422915",
            "51.4965527 -0.1417121",
            "51.4975816 -0.1419852",
            "51.4978693 -0.1424754",
            "51.4981047 -0.1437359",
            "51.498514 -0.1465631",
            "51.5017474 -0.1509025",
            "51.5048987 -0.1504423",
            "51.5126122 -0.15735",
            "51.5144898 -0.1615274",
            "51.5179559 -0.1666795",
            "51.5183755 -0.1668667"
        );

        if($this->pointInPolygon($point, $polygon, $pointOnVertex = true)=="inside"){
            return true;

        }
        return false;
    }

    function pointInPolygon($point, $polygon, $pointOnVertex = true) {
        $this->pointOnVertex = $pointOnVertex;

        // Transform string coordinates into arrays with x and y values
        $point = $this->pointStringToCoordinates($point);
        $vertices = array(); 
        foreach ($polygon as $vertex) {
            $vertices[] = $this->pointStringToCoordinates($vertex); 
        }

        // Check if the point sits exactly on a vertex
        if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
            return "inside";
        }

        // Check if the point is inside the polygon or on the boundary
        $intersections = 0; 
        $vertices_count = count($vertices);

        for ($i=1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i-1]; 
            $vertex2 = $vertices[$i];
            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) { // Check if point is on an horizontal polygon boundary
                return "boundary";
            }
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) { 
                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x']; 
                if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                    return "boundary";
                }
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++; 
                }
            } 
        } 
        // If the number of edges we passed through is odd, then it's in the polygon. 
        if ($intersections % 2 != 0) {
            return "inside";
        } else {
            return "outside";
        }
    }

    function pointOnVertex($point, $vertices) {
        foreach($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }

    }

    function pointStringToCoordinates($pointString) {
        $coordinates = explode(" ", $pointString);
        return array("x" => $coordinates[0], "y" => $coordinates[1]);
    }







 /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function airport_transfer_estimated_fare(Request $request){
        $loc_details=[];
        $min_waiting_time=0;
        $base_waiting_price=0;
        $this->validate($request,[
                's_latitude' => 'required|numeric',
                's_longitude' => 'required|numeric',
                'd_latitude' => 'required|numeric',
                'd_longitude' => 'required|numeric',
                'service_type' => 'required|numeric|exists:service_types,id',
            ]);
            try{
                $details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->s_latitude.",".$request->s_longitude."&destinations=".$request->d_latitude.",".$request->d_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');
                $json = curl($details);
                $details = json_decode($json, TRUE);
                $meter=0;
                $time=0;
                $seconds=0;
                foreach($details['rows'] as $key){
                    foreach($key as $abc){
                        foreach($abc as $googleElement){
                            $meter=$meter+($googleElement['distance']['value']);
                            $seconds=$seconds+($googleElement['duration']['value']);
                        }
                    }
                }
                
                $time = $details['rows'][0]['elements'][0]['duration']['text'];
                $kilometer = round(($meter/1000) / 1.609344);
                $sourceToDestinationDistance = round(($meter/1000),2);
                $minutes = round($seconds/60);
                $tax_percentage = Setting::get('tax_percentage');
                $commission_percentage = Setting::get('commission_percentage');
                $service_type = ServiceType::findOrFail($request->service_type);
                $price = $service_type->fixed;
                $min_waiting_time=$service_type->min_waiting_time;
                $base_waiting_price=$service_type->min_waiting_charge;
                if($service_type->calculator == 'MIN') {
                    $price += $service_type->minute * $minutes;
                } else if($service_type->calculator == 'HOUR') {
                    $price += $service_type->minute * 60;
                } else if($service_type->calculator == 'DISTANCE') {
                    $price += ($kilometer * $service_type->price);
                } else if($service_type->calculator == 'DISTANCEMIN') {
                    $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes);
                } else if($service_type->calculator == 'DISTANCEHOUR') {
                    $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes * 60);
                } else {
                    $price += ($kilometer * $service_type->price);
                }
                $tax_price = ( $tax_percentage/100 ) * $price;
                $total = $price + $tax_price;
                $nearest_driver_time = '';
                $nearest_driver_seconds = '';
                if($total < $service_type->min_price){
                    $total = $service_type->min_price; // minimum value for service type
                    $total_is_minimum = 1;
                }else{
                    $total_is_minimum = 0;
                }
                $total += $service_type->insure_price;
                
                //check for source 
                $rediusDistance=3.21;
                $Sourcelocations = LocationFee::select('*')
                ->select(DB::Raw("(6371 * acos( cos( radians('$request->s_latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$request->s_longitude') ) + sin( radians('$request->s_latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'locationfees.id','locationfees.location_name','locationfees.location_desc','locationfees.fee','locationfees.drop_fee')
                ->whereRaw("(6371 * acos( cos( radians('$request->s_latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$request->s_longitude') ) + sin( radians('$request->s_latitude') ) * sin( radians(latitude) ) ) ) <= $rediusDistance")
                ->orderBy('distance')
                ->get()->toArray();

                $Destinationlocations = LocationFee::select('*')
                ->select(DB::Raw("(6371 * acos( cos( radians('$request->d_latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$request->d_longitude') ) + sin( radians('$request->d_latitude') ) * sin( radians(latitude) ) ) ) AS distance"),'locationfees.id','locationfees.location_name','locationfees.location_desc','locationfees.fee','locationfees.drop_fee')
                ->whereRaw("(6371 * acos( cos( radians('$request->d_latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$request->d_longitude') ) + sin( radians('$request->d_latitude') ) * sin( radians(latitude) ) ) ) <= $rediusDistance")
                ->orderBy('distance')
                ->get()->toArray();


                
                $airport_pickup_fee=0;

                $airport_drop_fee=0;
                $isInCongesionZone=false;

                if($isInCongesionZone==false){
                    $isInCongesionZone =   $this->checkCongessions("$request->s_latitude $request->s_longitude");
                   }
       
                   if($isInCongesionZone==false){
                       $isInCongesionZone =   $this->checkCongessions("$request->d_latitude $request->d_longitude");
                      }

                foreach ($Destinationlocations as $value) {
                $airport_pickup_fee=(float)$value['fee'];
                $airport_drop_fee=(float)$value['drop_fee'];
            }

                foreach ($Sourcelocations as $value) {
                    $airport_pickup_fee=(float)$airport_pickup_fee+(float)$value['fee'];
                    $airport_drop_fee=(float)$airport_drop_fee+(float)$value['drop_fee'];
                    
                }
                $total += $airport_pickup_fee;
                $congesionZoneCharge=0;
                $cleanAirCharges=0;
    
    
                // add consgestion charger 
                if($isInCongesionZone==true){
                    $total=$total+15+12.50;
                    $congesionZoneCharge=15;
                    $cleanAirCharges=12.50;
    
                }
                
                if((float)$base_waiting_price>0){
                    $min_waiting_time= $min_waiting_time." minute";
                    $base_waiting_price=$base_waiting_price."/minute";
                }
                else
                {
                    $min_waiting_time= "N.A";
                    $base_waiting_price="N.A";
                }
            
                return response()->json([
                    'estimated_fare' => round($total,2), 
                    'distance' => $kilometer,
                    'sourceToDestinationDistance' =>  $sourceToDestinationDistance,
                    'time' => $time,
                    //'surge' => $surge,
                    //'surge_value' => '1.4X',
                    'tax_price' =>round( $tax_price,2),
                    'base_price' => $service_type->fixed,
                    'unit_time_pricing' => $service_type->minute,
                    //'wallet_balance' => Auth::user()->wallet_balance,
                    'insure_price' => $service_type->insure_price,
                     'airport_pickup_fee' => $airport_pickup_fee,
                     'airport_drop_fee'=>$airport_drop_fee,
                     'min_price' => round($service_type->min_price,2),
                     'total_is_minimum' => $total_is_minimum,
                     'currency'=>Setting::get('currency'),
                     "congesionZoneCharge"=>$congesionZoneCharge,
                     "cleanAirCharges"=>$cleanAirCharges

                    // 'nearest_driver_id' => $nearest_driver_id,
                   // 'nearest_driver_time' => $nearest_driver_time,
                   // 'nearest_driver_seconds' => $nearest_driver_seconds,
                   // 'min_waiting_time'=>$min_waiting_time,
                  //  'base_waiting_price'=>$base_waiting_price,
                    
                ]);

        } catch(Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }





    //Sorting distance array
    private static function sortByOrder($a, $b) { return ($b['kmd']<$a['kmd'])?1:-1; } 
    /**
     * Haversine Formula
     *
     * @return Distance between two points
     */
    public function getDistance( $s_latitude, $s_longitude, $latitude_c, $longitude_c ) {  
        $earth_radius = 6371;
    
        $dLat = deg2rad( $latitude_c - $s_latitude );  
        $dLon = deg2rad( $longitude_c - $s_longitude );  
    
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($s_latitude)) * cos(deg2rad($latitude_c)) * sin($dLon/2) * sin($dLon/2);  
        $c = 2 * asin(sqrt($a));  
        $d = $earth_radius * $c;  
    
        return $d;  
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function cancel_request(Request $request) {

        $this->validate($request, [
            'request_id' => 'required|numeric|exists:user_requests,id,user_id,'.Auth::user()->id,
        ]);

        try{

            $UserRequest = UserRequests::findOrFail($request->request_id);

            if($UserRequest->status == 'CANCELLED')
            {
                if($request->ajax()) {
                    return response()->json(['error' => trans('api.ride.already_cancelled')], 500); 
                }else{
                    return back()->with('flash_error', 'Request is Already Cancelled!');
                }
            }

            if(in_array($UserRequest->status, ['SEARCHING','STARTED','ARRIVED','SCHEDULED'])) {

                if($UserRequest->status != 'SEARCHING'){
                    $this->validate($request, [
                        'cancel_reason'=> 'max:255',
                    ]);
                }

                $UserRequest->status = 'CANCELLED';
                $UserRequest->cancel_reason = $request->cancel_reason;
                $UserRequest->cancelled_by = 'USER';
                $UserRequest->save();

                RequestFilter::where('request_id', $UserRequest->id)->delete();

                if($UserRequest->status != 'SCHEDULED'){

                    if($UserRequest->provider_id != 0){

                        ProviderService::where('provider_id',$UserRequest->provider_id)->update(['status' => 'active']);

                    }
                }

                 // Send Push Notification to User
                (new SendPushNotification)->UserCancellRide($UserRequest);

                if($request->ajax()) {
                    return response()->json(['message' => trans('api.ride.ride_cancelled')]); 
                }else{
                    return redirect('dashboard')->with('flash_success','Request Cancelled Successfully');
                }

            } else {
                if($request->ajax()) {
                    return response()->json(['error' => trans('api.ride.already_onride')], 500); 
                }else{
                    return back()->with('flash_error', 'Service Already Started!');
                }
            }
        }

        catch (ModelNotFoundException $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }else{
                return back()->with('flash_error', 'No Request Found!');
            }
        }

    }

    /**
     * Show the request status check.
     *
     * @return \Illuminate\Http\Response
     */

    public function request_status_check() {

        try{
            $check_status = ['CANCELLED', 'SCHEDULED'];

            $UserRequests = UserRequests::UserRequestStatusCheck(Auth::user()->id, $check_status)
                                        ->get()
                                        ->toArray();

                                      //  print_r((array)$UserRequests[0]['payment']); exit;
            if(array_key_exists(0,$UserRequests)){
                if(array_key_exists("payment",(array)$UserRequests[0])){
                   // print_r($UserRequests[0]['payment']); exit;
                    if(count((array)$UserRequests[0]['payment'])>0){
                        $fare=(float)((float)$UserRequests[0]['payment']['total']-(float)$UserRequests[0]['payment']['congestionCharge']-(float)$UserRequests[0]['payment']['cleanAirCharges']-(float)$UserRequests[0]['payment']['airport_fee']-(float)$UserRequests[0]['payment']['waiting_cost']);
                        $congestionCharge=(float)$UserRequests[0]['payment']['congestionCharge'];
                        $cleanAirCharges=(float)$UserRequests[0]['payment']['cleanAirCharges'];
                        $airport_fee=(float)$UserRequests[0]['payment']['airport_fee'];
                        $UserRequests[0]['payment']["breakUp"]=["fare"=>round((float)($fare),2),"congestionCharge"=>$congestionCharge,"cleanAirCharges"=>$cleanAirCharges,"airport_fee"=>$airport_fee];
                    
                    }
                }
            }                            


                                        return response()->json(['data' => $UserRequests]);

                if($UserRequests){
                    $providerId = $UserRequests[0]['provider']['id'];
                 
                //return $providerId
                $providerimage = ProviderDocument::where('provider_id', $providerId)
                ->where('document_id', 8)->first();

                if($providerimage){
                    $UserRequests[0]['provider']['vehicle_image'] = $providerimage;
                }else{
                    $UserRequests[0]['provider']['vehicle_image'] = (object)[];
                }
                }

            $search_status = ['SEARCHING','SCHEDULED'];
            $UserRequestsFilter = UserRequests::UserRequestAssignProvider(Auth::user()->id,$search_status)->get(); 

            // Log::info($UserRequestsFilter);

            $Timeout = Setting::get('provider_select_timeout', 180);

            if(!empty($UserRequestsFilter)){
                for ($i=0; $i < sizeof($UserRequestsFilter); $i++) {
                    $ExpiredTime = $Timeout - (time() - strtotime($UserRequestsFilter[$i]->assigned_at));
                    if($UserRequestsFilter[$i]->status == 'SEARCHING' && $ExpiredTime < 0) {
                        $Providertrip = new TripController();
                        $Providertrip->assign_next_provider($UserRequestsFilter[$i]->id);
                    }else if($UserRequestsFilter[$i]->status == 'SEARCHING' && $ExpiredTime > 0){
						
                        break;
                    }
                }
            }

            return response()->json(['data' => $UserRequests]);

        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function rate_provider(Request $request) {

        $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id,user_id,'.Auth::user()->id,
                'rating' => 'required|integer|in:1,2,3,4,5',
                'comment' => 'max:255',
            ]);
    
        $UserRequests = UserRequests::where('id' ,$request->request_id)
                ->where('status' ,'COMPLETED')
                ->where('paid', 0)
                ->first();

        if ($UserRequests) {
            if($request->ajax()){
                return response()->json(['error' => trans('api.user.not_paid')], 500);
            } else {
                return back()->with('flash_error', 'Service Already Started!');
            }
        }

        try{

            $UserRequest = UserRequests::findOrFail($request->request_id);
            
            if($UserRequest->rating == null) {
                UserRequestRating::create([
                        'provider_id' => $UserRequest->provider_id,
                        'user_id' => $UserRequest->user_id,
                        'request_id' => $UserRequest->id,
                        'user_rating' => $request->rating,
                        'user_comment' => $request->comment,
                    ]);
            } else {
                $UserRequest->rating->update([
                        'user_rating' => $request->rating,
                        'user_comment' => $request->comment,
                    ]);
            }

            $UserRequest->user_rated = 1;
            $UserRequest->save();

            $average = UserRequestRating::where('provider_id', $UserRequest->provider_id)->avg('user_rating');

            Provider::where('id',$UserRequest->provider_id)->update(['rating' => $average]);

            // Send Push Notification to Provider 
            if($request->ajax()){
                return response()->json(['message' => trans('api.ride.provider_rated')]); 
            }else{
                return redirect('dashboard')->with('flash_success', 'Driver Rated Successfully!');
            }
        } catch (Exception $e) {
            if($request->ajax()){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something went wrong');
            }
        }

    } 


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function trips() {
    try{
            $UserRequests = UserRequests::UserTrips(Auth::user()->id)->get();
            //return $UserRequests;
           
            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-end.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=640x260".
                            "&maptype=terrain".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:2|enc:".$value->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function estimated_fare(Request $request){
        //Log::info('Estimate', $request->all());
        $loc_details=[];
        $min_waiting_time=0;
        $base_waiting_price=0;
		//print_r($_REQUEST);exit;
        $this->validate($request,[
                's_latitude' => 'required|numeric',
                's_longitude' => 'required|numeric',
                'd_latitude' => 'required|numeric',
                'd_longitude' => 'required|numeric',
                'service_type' => 'required|numeric|exists:service_types,id',
            ]);
            $wayPointLatLog="";
            $isInCongesionZone=false;
            if($request->has('way_point')){
             $waypointArray=json_decode($request->way_point,true);   
           
            
           usort($waypointArray, function($a, $b) {return  strcmp($a['order'], $b['order']);});
            // $wayPointLatLog="&waypoints=";
             if(count($waypointArray)>0){
                 foreach($waypointArray as $key=>$val){
                     if($wayPointLatLog===""){
                       $wayPointLatLog=$val['lat'].','.$val['lng']."|";
                     }
                     else{
                       $wayPointLatLog=$wayPointLatLog.$val['lat'].','.$val['lng']."|";
                     }
                     if($isInCongesionZone==false){
                         $isInCongesionZone =   $this->checkCongessions($val['lat']." ".$val['lng']);
                    }
                }
            }
        }

         //
         if($isInCongesionZone==false){
             $isInCongesionZone =   $this->checkCongessions("$request->s_latitude $request->s_longitude");
            }

            if($isInCongesionZone==false){
                $isInCongesionZone =   $this->checkCongessions("$request->d_latitude $request->d_longitude");
               }

           
           
        try{
//AIzaSyDXM7F7wAc5GVGyOH3NBtPewu9mZo8mquE
if($wayPointLatLog!==""){ //echo  $wayPointLatLog; exit;
   $details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->s_latitude.",".$request->s_longitude."&destinations=".$wayPointLatLog.$request->d_latitude.",".$request->d_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');
}            
else{
    $details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->s_latitude.",".$request->s_longitude."&destinations=".$request->d_latitude.",".$request->d_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');
}
          //  $details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$request->s_latitude.",".$request->s_longitude."&destinations=".$request->d_latitude.",".$request->d_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');

            $json = curl($details);
            $details = json_decode($json, TRUE);

            $meter=0;
            $time=0;
            $seconds=0;
            foreach($details['rows'] as $key){
                foreach($key as $abc){
                    foreach($abc as $googleElement){
                       
                        $meter=$meter+($googleElement['distance']['value']);
                     //   $time=$time+($googleElement['duration']['text']);
                        $seconds=$seconds+($googleElement['duration']['value']);
                    }
                }
                
            }
            //$meter = $details['rows'][0]['elements'][0]['distance']['value'];
            $time = $details['rows'][0]['elements'][0]['duration']['text'];
            //$seconds = $details['rows'][0]['elements'][0]['duration']['value'];

            $kilometer = round(($meter/1000) / 1.609344); 
            $sourceToDestinationDistance = round(($meter/1000),2); 
            $minutes = round($seconds/60);

            $tax_percentage = Setting::get('tax_percentage');
            $commission_percentage = Setting::get('commission_percentage');
            $service_type = ServiceType::findOrFail($request->service_type);
            
            $price = $service_type->fixed;
            $min_waiting_time=$service_type->min_waiting_time;
            $base_waiting_price=$service_type->min_waiting_charge;

            if($service_type->calculator == 'MIN') {
                $price += $service_type->minute * $minutes;
            } else if($service_type->calculator == 'HOUR') {
                $price += $service_type->minute * 60;
            } else if($service_type->calculator == 'DISTANCE') {
                $price += ($kilometer * $service_type->price);
            } else if($service_type->calculator == 'DISTANCEMIN') {
                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes);
            } else if($service_type->calculator == 'DISTANCEHOUR') {
                $price += ($kilometer * $service_type->price) + ($service_type->minute * $minutes * 60);
            } else {
                $price += ($kilometer * $service_type->price);
            }

            $tax_price = ( $tax_percentage/100 ) * $price;
            $total = $price + $tax_price;
            $congesionZoneCharge=0;
            $cleanAirCharges=0;


            // add consgestion charger 
            if($isInCongesionZone==true){
                $total=$total+15+12.50;
                $congesionZoneCharge=15;
                $cleanAirCharges=12.50;

            }

            $ActiveProviders = ProviderService::AvailableServiceProvider($request->service_type)->get()->pluck('provider_id');

            $distance = Setting::get('provider_search_radius', '10');
            $latitude = $request->s_latitude;
            $longitude = $request->s_longitude;

            $Providers = Provider::whereIn('id', $ActiveProviders)
                ->where('status', 'approved')
                ->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                ->get();
			
                if(count($Providers)<=0)
                {

           			 return response()->json(['error' => trans('api.services_not_found')], 500);
      
                }
            $surge = 0;
            $nearest_driver_id = ($Providers[0]->id) ? $Providers[0]->id : '';
            
            if(!empty($nearest_driver_id)){
                $nearest_driver_lat = $Providers[0]->latitude;
                $nearest_driver_long = $Providers[0]->longitude;
                
                $nearest_driver_details = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$nearest_driver_lat.",".$nearest_driver_long."&destinations=".$request->s_latitude.",".$request->s_longitude."&mode=driving&sensor=false&key=".env('GOOGLE_MAP_KEY');
                $json = curl($nearest_driver_details);
                $nearest_driver_details = json_decode($json, TRUE);
                
                $nearest_driver_time = $nearest_driver_details['rows'][0]['elements'][0]['duration']['text'];
                $nearest_driver_seconds = $nearest_driver_details['rows'][0]['elements'][0]['duration']['value'];
                
            }else{
                $nearest_driver_time = '';
                $nearest_driver_seconds = '';
            }
            
            if($Providers->count() <= Setting::get('surge_trigger') && $Providers->count() > 0){
                $surge_price = (Setting::get('surge_percentage')/100) * $total;
                $total += $surge_price;
                $surge = 1;
            }
            if($total < $service_type->min_price){
                $total = $service_type->min_price; // minimum value for service type
                $total_is_minimum = 1;
            }else{
                $total_is_minimum = 0;
            }

            $total += $service_type->insure_price;
           
            //Airport fee calc
            $locations = LocationFee::orderBy('created_at' , 'desc')->get()->toArray();           
            foreach ($locations as $value) {
                $loc_details[] = array(
                    'kmd' => $this->getDistance($latitude, $longitude, $value['latitude'], $value['longitude']),
                    'fee' => $value['fee'],
                );
            }
            // $all_kmd = array_column($loc_details, 'kmd');
            // $min_distance = min($all_kmd);
            $user_airport_fee=0;
            if(!empty($loc_details)){
                usort($loc_details, array('App\Http\Controllers\UserApiController','sortByOrder'));
                $user_airport_fee = ($loc_details[0]['kmd'] * 1000 <= 1000) ? $loc_details[0]['fee'] : 0 ;
            }
            $total += $user_airport_fee;
           
            if((float)$base_waiting_price>0){
                $min_waiting_time= $min_waiting_time." minute";
                $base_waiting_price=$base_waiting_price."/minute";
            }
            else
            {
                $min_waiting_time= "N.A";
                $base_waiting_price="N.A";
            }
            

            return response()->json([
                    'estimated_fare' => round($total,2), 
                    'distance' => $kilometer,
                    'sourceToDestinationDistance' =>  $sourceToDestinationDistance,
                    'time' => $time,
                    'surge' => $surge,
                    'surge_value' => '1.4X',
                    'tax_price' => $tax_price,
                    'base_price' => $service_type->fixed,
                    'unit_time_pricing' => $service_type->minute,
                    'wallet_balance' => Auth::user()->wallet_balance,
                    'insure_price' => $service_type->insure_price,
                    'airport_fee' => $user_airport_fee,
                    'min_price' => $service_type->min_price,
                    'total_is_minimum' => $total_is_minimum,
                    'nearest_driver_id' => $nearest_driver_id,
                    'nearest_driver_time' => $nearest_driver_time,
                    'nearest_driver_seconds' => $nearest_driver_seconds,
                    'min_waiting_time'=>$min_waiting_time,
                    'base_waiting_price'=>$base_waiting_price,
                    'congestionCharge'=>$congesionZoneCharge,
                    'cleanAirCharges'=>$cleanAirCharges
                ]);

        } catch(Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function trip_details(Request $request) {

         $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);
    
        try{
            $UserRequests = UserRequests::UserTripDetails(Auth::user()->id,$request->request_id)->get();
            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-end.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=640x260".
                            "&maptype=roadmap".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:2|enc:".$value->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }

    /**
     * get all promo code.
     *
     * @return \Illuminate\Http\Response
     */

    public function promocodes() {
        try{
            $this->check_expiry();

            return PromocodeUsage::Active()
                    ->where('user_id', Auth::user()->id)
                    ->with('promocode')
                    ->get();

        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    } 


    public function check_expiry(){
        try{
            $Promocode = Promocode::all();
            foreach ($Promocode as $index => $promo) {
                if(date("Y-m-d") > $promo->expiration){
                    $promo->status = 'EXPIRED';
                    $promo->save();
                    PromocodeUsage::where('promocode_id', $promo->id)->update(['status' => 'EXPIRED']);
                }else{
                    PromocodeUsage::where('promocode_id', $promo->id)->update(['status' => 'ADDED']);
                }
            }
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }


    /**
     * add promo code.
     *
     * @return \Illuminate\Http\Response
     */

    public function add_promocode(Request $request) {

        $this->validate($request, [
                'promocode' => 'required|exists:promocodes,promo_code',
            ]);

        try{

            $find_promo = Promocode::where('promo_code',$request->promocode)->first();

            if($find_promo->status == 'EXPIRED' || (date("Y-m-d") > $find_promo->expiration)){

                if($request->ajax()){

                    return response()->json([
                        'message' => trans('api.promocode_expired'), 
                        'code' => 'promocode_expired'
                    ]);

                }else{
                    return back()->with('flash_error', trans('api.promocode_expired'));
                }

            }elseif(PromocodeUsage::where('promocode_id',$find_promo->id)->where('user_id', Auth::user()->id)->where('status','ADDED')->count() > 0){
                // return response()->json([
                //     'message' => 'ok', 
                //     'PromocodeUsage' => PromocodeUsage::where('promocode_id',$find_promo->id)->where('user_id', Auth::user()->id)->where('status','ADDED')->get()
                //     ]);
                if($request->ajax()){

                    return response()->json([
                        'message' => trans('api.promocode_already_in_use'), 
                        'code' => 'promocode_already_in_use'
                        ]);

                }else{
                    return back()->with('flash_error', 'Promocode Already in use');
                }

            }else{

                $promo = new PromocodeUsage;
                $promo->promocode_id = $find_promo->id;
                $promo->user_id = Auth::user()->id;
                $promo->status = 'ADDED';
                $promo->save();

                if($request->ajax()){

                    return response()->json([
                            'message' => trans('api.promocode_applied') ,
                            'code' => 'promocode_applied'
                         ]); 

                }else{
                    return back()->with('flash_success', trans('api.promocode_applied'));
                }
            }

        }

        catch (Exception $e) {
            if($request->ajax()){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something Went Wrong');
            }
        }

    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function upcoming_trips() {
    
        try{
            $UserRequests = UserRequests::UserUpcomingTrips(Auth::user()->id)->get();
            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-end.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=640x260".
                            "&maptype=terrain".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:2|enc:".$value->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function upcoming_trip_details(Request $request) {

         $this->validate($request, [
                'request_id' => 'required|integer|exists:user_requests,id',
            ]);
    
        try{
            $UserRequests = UserRequests::UserUpcomingTripDetails(Auth::user()->id,$request->request_id)->get();
            if(!empty($UserRequests)){
                $map_icon = asset('asset/img/marker-end.png');
                foreach ($UserRequests as $key => $value) {
                    $UserRequests[$key]->static_map = "https://maps.googleapis.com/maps/api/staticmap?".
                            "autoscale=1".
                            "&size=640x260".
                            "&maptype=roadmap".
                            "&format=png".
                            "&visual_refresh=true".
                            "&markers=icon:".$map_icon."%7C".$value->s_latitude.",".$value->s_longitude.
                            "&markers=icon:".$map_icon."%7C".$value->d_latitude.",".$value->d_longitude.
                            "&path=color:0x000000|weight:2|enc:".$value->route_key.
                            "&key=".env('GOOGLE_MAP_KEY');
                }
            }
            return $UserRequests;
        }

        catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }


    /**
     * Show the nearby providers.
     *
     * @return \Illuminate\Http\Response
     */

    public function show_providers(Request $request) {

        $this->validate($request, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
                'service' => 'numeric|exists:service_types,id',
            ]);

        try{

            $distance = Setting::get('provider_search_radius', '10');
            $latitude = $request->latitude;
            $longitude = $request->longitude;

            if($request->has('service')){
                
                $ActiveProviders = ProviderService::AvailableServiceProvider($request->service)->get()->pluck('provider_id');
                $Providers = Provider::whereIn('id', $ActiveProviders)
                    ->where('status', 'approved')
                    ->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                    ->get();
            } else {
                $ActiveProviders = ProviderService::AvailableServiceProviderActive()->get()->pluck('provider_id');
             /*   print_r($ActiveProviders); exit;
                $Providers = Provider::whereIn('id', $ActiveProviders)
                    ->where('status', 'approved')
                    ->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                    ->get(); */
                $Providers = Provider::whereIn('id', $ActiveProviders)
                ->where('status', 'approved')
                    ->whereRaw("(1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                    ->get();
                    
            }

            if(count($Providers) == 0) {
                if($request->ajax()) {
                    return response()->json(['message' => "No Providers Found"]); 
                }else{
                    return back()->with('flash_success', 'No Providers Found! Please try again.');
                }
            }
        
            return $Providers;

        } catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
            }else{
                return back()->with('flash_error', 'Something went wrong while sending request. Please try again.');
            }
        }
    }


    /**
     * Forgot Password.
     *
     * @return \Illuminate\Http\Response
     */


    public function forgot_password(Request $request){
      // echo 1 ; exit;
        $this->validate($request, [
                'email' => 'required|email',
            ]);

        try{  


            $userArray = User::where('email' , $request->email)->where('login_by',"manual")->get()->toArray();
            if(count($userArray)==0){
                return response()->json(['message' => trans('api.email_not_reg')], 400);
            }

            
            $user = User::where('email' , $request->email)->first();

            

            $otp = mt_rand(100000, 999999);

            $user->otp = $otp;

            

            $user->save();
            Notification::send($user, new ResetPasswordOTP($otp));
            return response()->json([
                'message' => 'OTP sent to your email!',
                'user' => $user
            ]);
        //    $user->notify(new ResetPasswordOTP($otp));


            //

            

        }catch(Exception $e){
                return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

    public function send_otp(Request $request){
        try{
            $this->validate($request, [
                'mobile_no' => 'required|max:10',
                'isdCode'=>'required'
                ]);
            //send otp.
            $data=(object)[];
            $dateOfGen=date('Y-m-d H:i:s');
            $digits_otp = 4;
            $otp= rand(pow(10, $digits_otp-1), pow(10, $digits_otp)-1);
            
            $data->body="Your one time password to reset your account password is ".$otp;
            $data->otp=$otp;
            $data->isdCode=$request->isdCode;
            $data->mobile_no=$request->mobile_no;  
            
            $TwilioSMS=new TwilioSMS();
            $TwilioSMSReturn=$TwilioSMS->accessSendSMS($data);
            return response()->json(['message' => $TwilioSMSReturn['message'],"data"=>(object)["otp"=>$otp]],$TwilioSMSReturn['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }


    /**
     * Reset Password.
     *
     * @return \Illuminate\Http\Response
     */

    public function reset_password(Request $request){

        $this->validate($request, [
                'password' => 'required|confirmed|min:6',
                'id' => 'required|numeric|exists:users,id'
            ]);

        try{

            $User = User::findOrFail($request->id);
            $User->password = bcrypt($request->password);
            $User->save();

            if($request->ajax()) {
                return response()->json(['message' => 'Password Updated']);
            }

        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }
        }
    }

    /**
     * help Details.
     *
     * @return \Illuminate\Http\Response
     */

    public function help_details(Request $request){

        try{

            if($request->ajax()) {
                return response()->json([
                    'contact_number' => Setting::get('contact_number',''), 
                    'contact_email' => Setting::get('contact_email','')
                     ]);
            }

        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
            }
        }
    }

    /**
     * Add to user favourite ride
     *
     * @return \Illuminate\Http\Response
     */
    public function add_favourite(Request $request) {

        $UserRequest = UserRequests::findOrFail($request->id);
        try{
            //if($request->ajax()) {
                if($UserRequest->status == 'COMPLETED') {
                    $UserRequest->is_fav = 1;
                    $UserRequest->save();
                }
                return response()->json(['favdone' => "Added to Favourite"]);
            //}

        }catch (Exception $e) {
            //if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
           // }
        }
    }

    /**
     * Remove from user favourite ride
     *
     * @return \Illuminate\Http\Response
     */
    public function remove_favourite(Request $request) {

        $UserRequest = UserRequests::findOrFail($request->id);
        try{
            //if($request->ajax()) {
                if($UserRequest->status == 'COMPLETED') {
                    $UserRequest->is_fav = 0;
                    $UserRequest->save();
                }
                return response()->json(['favremoved' => "Removed from Favourites"]);
            //}

        }catch (Exception $e) {
            //if($request->ajax()) {
                return response()->json(['error' => trans('api.something_went_wrong')]);
           // }
        }
    }


    public function UpdateDeviceToken(Request $request)
    {

    	$device_token=$request->device_token;
    	  $user = User::findOrFail(Auth::user()->id);
    	
    	 try {
          if($request->has('device_token')){ 
                $user->device_token = $device_token;
                 $user->save();
                 return response()->json(['message' => "Device Token Updated"]);
            }
            
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }


    public function send_test_push()
    {
        try{
//echo 'ljhfg';die;
dump((new SendPushNotification)->ProviderNotAvailable(3));
exit;
            $message = \PushNotification::Message('hi',array(
                'badge' => 1,
                'sound' => 'default',

                /*'title' => $request['title'],
                'body' => $request['description'], 
                'tag' => '1',
                'click_action' => 'OPEN_ACTIVITY_1',*/
                //'icon' => 'fcm_push_icon'
                
                'actionLocKey' => 'Bliss Cars 24 Ride',
                'locKey' => 'localized key',
                'locArgs' => array(
                    'localized args',
                    'localized args',
                ),
                'launchImage' => 'image.jpg',
                'description' => 'blisscars',
                'custom' => array('custom_data' => array(
                    'bliss' => 'BlissCars24News', 'News'
                ))
            ));

        \PushNotification::app([
            'environment' => 'production',
        'apiKey'      => 'AAAA03siFwI:APA91bHgfxZvtv8j2DTL5CwLeZXGtnS-meMvS7S8STDWgdUap-Cz8jxzRezwY4gmxlpSQe_41SoGNJcfexTie25U_qvnnAw3WVPs3_2pOqN61jjal_Jg9aupkXxO1iRrboqxEmFGdbd6',
        'service'     => 'fcm'
        ])
        ->to('ewJzAVpZ8vE:APA91bGSEfQglfNA5SPFQXnxDEM6GRmAEhjGp6m0eCwGIWpz8WYnsX2owaAtr4-IKiWY_PemRLNdH7XrDuXWzHnl7gYpw5r85qDclQZkmdp2fVUXaBo5ENa6Kwa_8tSEru9zl9IJSqwd')
        ->send($message);


        return response()->json(['msg' => 'ok']);
        }
        catch(\Exception $e){
            return response()->json(['error' => $e->getMessage()]);
        }
     
    }

}
