<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverTowardsDestination extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id',
        's_lat',
		 's_lng',
		'd_lng',
		'd_lat',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];  

    public function scopeQueue($query)
    {
        return $query->orderBy('id','ASC');
    }

}
