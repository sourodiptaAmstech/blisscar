<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locationfee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location_name',
        'location_desc',
        'fee',
        'latitude',
        'longitude',
        'place_id',
        'drop_fee'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
