<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderWalletHistory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id','created_at', 'transaction_id', 'status','start_date','end_date','duration','plan_id'
   ];


   /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];


    public function provider()
    {
        return $this->belongsTo('App\Provider');
    }

    /**
     * The wallets that belong to the plan.
     */

     public function plan(){
         return $this->belongsTo('App\WalletPlan');
     }

}
